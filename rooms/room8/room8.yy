
{
    "name": "room8",
    "id": "1f4ebce5-6dc6-4455-a017-3886ee68e792",
    "creationCodeFile": "",
    "inheritCode": false,
    "inheritCreationOrder": false,
    "inheritLayers": false,
    "instanceCreationOrderIDs": [
        "c9a31db7-8b00-47ab-a61c-6f7d1d08ea89",
        "ecf3865a-52d5-4abf-ae39-e7541d4e7a38",
        "17f126de-ef70-43b4-8fa2-14be88b022dc",
        "e9ea4382-4244-47df-869c-f5a122df0637",
        "5bc67c0e-d9c2-4381-a855-86bd6e0fa0fd",
        "6007cd68-c1f2-49fe-98e1-4fe58323d0f1",
        "ef2ba520-560c-419b-98de-87b1fdacc8bd",
        "2197e716-9f9b-4a7f-ae2c-442186118e51",
        "56cf3ccf-a2d7-4ed2-9830-6466a9b11bb1"
    ],
    "IsDnD": false,
    "layers": [
        {
            "__type": "GMRInstanceLayer_Model:#YoYoStudio.MVCFormat",
            "name": "Instances",
            "id": "26ea713a-516c-473d-9d7c-e18c47490460",
            "depth": 0,
            "grid_x": 32,
            "grid_y": 32,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "inheritLayerDepth": false,
            "inheritLayerSettings": false,
            "inheritSubLayers": false,
            "inheritVisibility": false,
            "instances": [
                {"name": "inst_1D96032A","id": "c9a31db7-8b00-47ab-a61c-6f7d1d08ea89","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_1D96032A","objId": "ac860cbe-e23d-42a7-95ef-4b9d8c6235c2","rotation": 0,"scaleX": 4,"scaleY": 1,"mvc": "1.0","x": 1024,"y": 0},
                {"name": "inst_7D98A3E","id": "ecf3865a-52d5-4abf-ae39-e7541d4e7a38","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_7D98A3E","objId": "4af3ba30-219b-474e-ab87-2d068559951a","rotation": 0,"scaleX": 1,"scaleY": 4,"mvc": "1.0","x": 1984,"y": 512},
                {"name": "inst_377A4015","id": "17f126de-ef70-43b4-8fa2-14be88b022dc","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_377A4015","objId": "51184f55-7a6b-4e00-a4fa-809c3eba48c0","rotation": 0,"scaleX": 8,"scaleY": 3,"mvc": "1.0","x": 1024,"y": 1440},
                {"name": "inst_1DA1F8C6","id": "e9ea4382-4244-47df-869c-f5a122df0637","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_1DA1F8C6","objId": "117ffa30-a4ec-4398-b248-fc4979bba1a2","rotation": 0,"scaleX": 32,"scaleY": 14,"mvc": "1.0","x": 0,"y": 64},
                {"name": "inst_4A36CC42","id": "5bc67c0e-d9c2-4381-a855-86bd6e0fa0fd","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_4A36CC42","objId": "117ffa30-a4ec-4398-b248-fc4979bba1a2","rotation": 0,"scaleX": 24,"scaleY": 14,"mvc": "1.0","x": 1280,"y": 64},
                {"name": "inst_2186A0D4","id": "6007cd68-c1f2-49fe-98e1-4fe58323d0f1","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_2186A0D4","objId": "117ffa30-a4ec-4398-b248-fc4979bba1a2","rotation": 0,"scaleX": 32,"scaleY": 21,"mvc": "1.0","x": 0,"y": 768},
                {"name": "inst_1311E219","id": "ef2ba520-560c-419b-98de-87b1fdacc8bd","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_1311E219","objId": "117ffa30-a4ec-4398-b248-fc4979bba1a2","rotation": 0,"scaleX": 24,"scaleY": 21,"mvc": "1.0","x": 1280,"y": 768},
                {"name": "inst_2BDA5913","id": "2197e716-9f9b-4a7f-ae2c-442186118e51","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_2BDA5913","objId": "117ffa30-a4ec-4398-b248-fc4979bba1a2","rotation": 0,"scaleX": 2,"scaleY": 8,"mvc": "1.0","x": -64,"y": 512},
                {"name": "inst_29FB8A40","id": "56cf3ccf-a2d7-4ed2-9830-6466a9b11bb1","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_29FB8A40","objId": "7f41b62b-1d7a-434f-8f7c-7013849e45c2","rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","x": 1600,"y": 1120}
            ],
            "layers": [

            ],
            "m_parentID": "00000000-0000-0000-0000-000000000000",
            "m_serialiseFrozen": false,
            "modelName": "GMRInstanceLayer",
            "mvc": "1.0",
            "userdefined_depth": false,
            "visible": true
        },
        {
            "__type": "GMRTileLayer_Model:#YoYoStudio.MVCFormat",
            "name": "Tiles_1",
            "id": "a5549bac-d2f4-454b-80fc-32c51d335d3d",
            "depth": 100,
            "grid_x": 32,
            "grid_y": 32,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "inheritLayerDepth": false,
            "inheritLayerSettings": false,
            "inheritSubLayers": false,
            "inheritVisibility": false,
            "layers": [

            ],
            "m_parentID": "00000000-0000-0000-0000-000000000000",
            "m_serialiseFrozen": false,
            "modelName": "GMRTileLayer",
            "prev_tileheight": 256,
            "prev_tilewidth": 256,
            "mvc": "1.0",
            "tiles": {
                "SerialiseData": null,
                "SerialiseHeight": 6,
                "SerialiseWidth": 8,
                "TileSerialiseData": [
                    7,7,7,7,1,7,7,7,
                    13,13,13,13,1,13,13,13,
                    1,1,1,1,1,1,1,1,
                    7,7,7,7,1,7,7,7,
                    13,13,13,13,1,13,13,13,
                    13,13,13,13,1,13,13,13
                ]
            },
            "tilesetId": "ebc37809-7608-44ff-9d90-60a814f4584c",
            "userdefined_depth": false,
            "visible": true,
            "x": 0,
            "y": 0
        },
        {
            "__type": "GMRBackgroundLayer_Model:#YoYoStudio.MVCFormat",
            "name": "Background",
            "id": "2d5bc973-f689-4d9d-824e-10fa008f9f1b",
            "animationFPS": 15,
            "animationSpeedType": "0",
            "colour": { "Value": 4290822336 },
            "depth": 200,
            "grid_x": 32,
            "grid_y": 32,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "hspeed": 0,
            "htiled": false,
            "inheritLayerDepth": false,
            "inheritLayerSettings": false,
            "inheritSubLayers": false,
            "inheritVisibility": false,
            "layers": [

            ],
            "m_parentID": "00000000-0000-0000-0000-000000000000",
            "m_serialiseFrozen": false,
            "modelName": "GMRBackgroundLayer",
            "mvc": "1.0",
            "spriteId": "00000000-0000-0000-0000-000000000000",
            "stretch": false,
            "userdefined_animFPS": false,
            "userdefined_depth": false,
            "visible": true,
            "vspeed": 0,
            "vtiled": false,
            "x": 0,
            "y": 0
        }
    ],
    "modelName": "GMRoom",
    "parentId": "00000000-0000-0000-0000-000000000000",
    "physicsSettings":     {
        "id": "d60ecd42-bd06-4314-9aee-b55c1e73b966",
        "inheritPhysicsSettings": false,
        "modelName": "GMRoomPhysicsSettings",
        "PhysicsWorld": false,
        "PhysicsWorldGravityX": 0,
        "PhysicsWorldGravityY": 10,
        "PhysicsWorldPixToMeters": 0.1,
        "mvc": "1.0"
    },
    "roomSettings":     {
        "id": "38fa7b14-f00d-41c6-a2b2-4f6cf88a3309",
        "Height": 1536,
        "inheritRoomSettings": false,
        "modelName": "GMRoomSettings",
        "persistent": false,
        "mvc": "1.0",
        "Width": 2048
    },
    "mvc": "1.0",
    "views": [
        {"id": "f8ed1414-cf86-471a-b97d-3ec6ec310caf","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
        {"id": "49945b04-3254-478b-b484-0c5ddb10d829","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
        {"id": "bf4e06a2-9c23-49fb-8548-a932f3dc6c0b","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
        {"id": "62c927c3-da1a-4472-b2c7-1cd400e40d74","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
        {"id": "ccf66972-27b8-464e-8745-970b68fd5048","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
        {"id": "e158d7f3-4172-4389-ad77-0a4b753cc445","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
        {"id": "1a79c9d3-a219-47da-9979-91b29c1a5d4c","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
        {"id": "f5032688-27e9-4242-acfc-aabd65e0a4b6","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0}
    ],
    "viewSettings":     {
        "id": "04bf65d8-546c-4ae9-9ddc-71fdf7e52b26",
        "clearDisplayBuffer": true,
        "clearViewBackground": false,
        "enableViews": false,
        "inheritViewSettings": false,
        "modelName": "GMRoomViewSettings",
        "mvc": "1.0"
    }
}