Megan R, Khendra, Lori

Lost

How to play - 
solve the mini games with the mouse
in the mazes use arrow keys to move around

Cheat code 
Each room can be skipped by pressing Q.

See IMG_20171130_204430 for the solution to the first mini game.

See "insert Img file name here" for the maze of rooms solution.

To beat the last level get the boss to collide with the active traps (yellow walls), three times, by colliding with the inactive wall(purple wall) then when it turns to a set wall(pink wall) collide again.

