active = false;
ob_boss_maze_robot_player.life -= 1;
if ob_boss_maze_robot_player.life == 0
{
	instance_destroy(ob_boss_maze_robot_player);
	var inst;
	inst = instance_create_layer(x, y, "Instances", ob_message_maze_death);

	with (inst)
	{
		str_message = "You're Dead!";
		time = 0;
	}
}