{
    "id": "87829799-7749-49b7-af05-39dc2411a183",
    "modelName": "GMTimeline",
    "mvc": "1.0",
    "name": "active_trap_timeline_boss",
    "momentList": [
        {
            "id": "73cfc4f4-0895-48c8-b61c-7d59627c8a97",
            "modelName": "GMMoment",
            "mvc": "1.0",
            "name": "",
            "evnt": {
                "id": "3032c212-27e4-4b2f-bb10-eb9a4932ca79",
                "modelName": "GMEvent",
                "mvc": "1.0",
                "IsDnD": false,
                "collisionObjectId": "00000000-0000-0000-0000-000000000000",
                "enumb": 0,
                "eventtype": 0,
                "m_owner": "87829799-7749-49b7-af05-39dc2411a183"
            },
            "moment": 0
        },
        {
            "id": "350d1114-4dc2-4410-a673-2d9e9a2d5948",
            "modelName": "GMMoment",
            "mvc": "1.0",
            "name": "",
            "evnt": {
                "id": "b649c649-d4dd-4983-9812-0af499254d51",
                "modelName": "GMEvent",
                "mvc": "1.0",
                "IsDnD": false,
                "collisionObjectId": "00000000-0000-0000-0000-000000000000",
                "enumb": 20,
                "eventtype": 0,
                "m_owner": "87829799-7749-49b7-af05-39dc2411a183"
            },
            "moment": 20
        }
    ]
}