{
    "id": "a5a121e2-b0d0-484a-a289-e7774451ec5c",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "font_message",
    "AntiAlias": 1,
    "TTFName": "",
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "System Font",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "dd6c8e80-f58c-401e-bdab-b55de28322f4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 47,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 261,
                "y": 149
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "b2889a8c-cf81-4d66-91e6-2d3a3d10664d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 47,
                "offset": 3,
                "shift": 10,
                "w": 4,
                "x": 309,
                "y": 149
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "8bed9261-02d0-42f3-b512-09836dfdce5a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 47,
                "offset": 3,
                "shift": 16,
                "w": 11,
                "x": 157,
                "y": 149
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "2c8024b4-209b-44c6-b6a5-c2fb7b8ab6c5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 47,
                "offset": 0,
                "shift": 24,
                "w": 25,
                "x": 281,
                "y": 2
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "973531d6-c357-4968-a363-8b78e46ac723",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 47,
                "offset": 2,
                "shift": 25,
                "w": 21,
                "x": 148,
                "y": 51
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "3f206ab7-f8a1-4aeb-991b-cff810db7258",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 47,
                "offset": 1,
                "shift": 31,
                "w": 29,
                "x": 106,
                "y": 2
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "1130dd44-24f5-49b7-8ca5-b144398fa2b2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 47,
                "offset": 2,
                "shift": 27,
                "w": 26,
                "x": 253,
                "y": 2
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "73fb68b4-d7a4-4d3f-99cb-3fe1250f1ea4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 47,
                "offset": 3,
                "shift": 9,
                "w": 4,
                "x": 315,
                "y": 149
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "ed9825e0-8321-4e43-9606-9d136bab524d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 47,
                "offset": 4,
                "shift": 13,
                "w": 9,
                "x": 239,
                "y": 149
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "3b07cab4-1ee5-4249-867a-4edc98b2cc14",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 47,
                "offset": 1,
                "shift": 13,
                "w": 9,
                "x": 250,
                "y": 149
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "5b0365ee-b970-4d65-9319-d27f4ad64e08",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 47,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 37,
                "y": 149
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "ea0b4b99-5be1-4720-b0be-00354a53254a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 47,
                "offset": 3,
                "shift": 25,
                "w": 19,
                "x": 212,
                "y": 100
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "c420ab26-eb7e-42fc-bb6e-2f1fbdbf5ad6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 47,
                "offset": 1,
                "shift": 10,
                "w": 7,
                "x": 271,
                "y": 149
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "f2c05962-ff8d-4814-a76a-6d4d801f3479",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 47,
                "offset": 2,
                "shift": 17,
                "w": 13,
                "x": 69,
                "y": 149
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "6b781300-d082-4b50-bd3b-16f0749eb0d3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 47,
                "offset": 3,
                "shift": 10,
                "w": 4,
                "x": 321,
                "y": 149
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "8d5e5778-b6cf-4882-bed6-f1bd0280794d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 47,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 99,
                "y": 149
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "b6a98b96-1676-4099-a098-c006d113d9a8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 47,
                "offset": 2,
                "shift": 24,
                "w": 21,
                "x": 125,
                "y": 51
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "a69950d4-84b9-4450-b4d0-0498aa795376",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 47,
                "offset": 1,
                "shift": 17,
                "w": 12,
                "x": 129,
                "y": 149
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "852f1e79-dc0e-4d19-8dcb-5fca06e4d36d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 47,
                "offset": 2,
                "shift": 23,
                "w": 19,
                "x": 483,
                "y": 51
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "56efa5bd-b322-43ea-a097-e5d742dfb9ad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 47,
                "offset": 2,
                "shift": 24,
                "w": 20,
                "x": 376,
                "y": 51
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "8abe8c67-efcf-48a3-b670-a744177993e6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 47,
                "offset": 1,
                "shift": 24,
                "w": 22,
                "x": 77,
                "y": 51
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "c678f4f2-0ce7-44dc-8606-868c021c0edd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 47,
                "offset": 2,
                "shift": 23,
                "w": 20,
                "x": 398,
                "y": 51
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "e7572d29-1358-412f-be09-bb92a008a40b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 47,
                "offset": 2,
                "shift": 24,
                "w": 21,
                "x": 194,
                "y": 51
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "16d7e3ff-b69c-486a-97f8-8d530d7624ee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 47,
                "offset": 1,
                "shift": 22,
                "w": 19,
                "x": 191,
                "y": 100
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "c5eec0f4-0707-4a52-b1b8-22494bd68489",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 47,
                "offset": 2,
                "shift": 24,
                "w": 21,
                "x": 217,
                "y": 51
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "5f121abd-67f5-49a7-be60-a0d48390f309",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 47,
                "offset": 1,
                "shift": 23,
                "w": 21,
                "x": 240,
                "y": 51
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "f127651a-cdcc-4ff2-ad34-5c770daf29dd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 47,
                "offset": 3,
                "shift": 10,
                "w": 4,
                "x": 333,
                "y": 149
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "44eaa9ee-6d9b-43f0-9fd7-4c6e918f8d64",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 47,
                "offset": 1,
                "shift": 10,
                "w": 6,
                "x": 289,
                "y": 149
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "8b41fc6b-91bd-4b16-b8c8-e605c35b5ace",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 47,
                "offset": 3,
                "shift": 25,
                "w": 19,
                "x": 149,
                "y": 100
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "b3dabb52-e333-4bee-9057-6f3d2fe54fce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 47,
                "offset": 3,
                "shift": 24,
                "w": 18,
                "x": 313,
                "y": 100
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "83ba23ac-62c6-43ce-b56f-f8afc8b33a36",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 47,
                "offset": 3,
                "shift": 25,
                "w": 19,
                "x": 420,
                "y": 51
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "ad50ac7c-e7bc-41ac-b5fe-2abe23687953",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 47,
                "offset": 1,
                "shift": 20,
                "w": 17,
                "x": 428,
                "y": 100
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "595d8ed7-d506-4daf-94b8-00830b948595",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 47,
                "offset": 2,
                "shift": 35,
                "w": 32,
                "x": 41,
                "y": 2
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "36024074-27c5-475d-9dd2-26826d9ff16c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 47,
                "offset": 0,
                "shift": 25,
                "w": 25,
                "x": 416,
                "y": 2
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "b4daee33-ed79-4d35-91fa-ee245a4c49e3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 47,
                "offset": 3,
                "shift": 24,
                "w": 20,
                "x": 332,
                "y": 51
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "bc8f3e1f-6e09-4b82-b24f-b6e863007e73",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 47,
                "offset": 2,
                "shift": 28,
                "w": 25,
                "x": 362,
                "y": 2
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "7fbab6af-14c8-4b69-a47f-012de4d20ddf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 47,
                "offset": 3,
                "shift": 27,
                "w": 23,
                "x": 27,
                "y": 51
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "a40afe00-eecc-4231-9d30-9beee15ce4f0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 47,
                "offset": 3,
                "shift": 22,
                "w": 18,
                "x": 293,
                "y": 100
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "76e0fabe-16d1-49ac-b1e1-157bcf9143c7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 47,
                "offset": 3,
                "shift": 21,
                "w": 17,
                "x": 352,
                "y": 100
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "cc0bd9c1-688f-46ab-a916-b1d48a0ad995",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 47,
                "offset": 2,
                "shift": 29,
                "w": 25,
                "x": 389,
                "y": 2
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "61a914f1-dd1c-4744-9094-093722035098",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 47,
                "offset": 3,
                "shift": 28,
                "w": 23,
                "x": 2,
                "y": 51
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "00ec1cde-dcb2-49e1-af64-06b276f47c13",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 47,
                "offset": 3,
                "shift": 9,
                "w": 4,
                "x": 297,
                "y": 149
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "16f6f80c-c44f-4b86-97f3-cd052d3d9b7d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 47,
                "offset": 1,
                "shift": 20,
                "w": 16,
                "x": 2,
                "y": 149
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "3e054d4f-28d7-4401-ac25-17a4855835da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 47,
                "offset": 3,
                "shift": 25,
                "w": 22,
                "x": 101,
                "y": 51
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "c87e305e-c1ab-41ac-9c6b-692f210d417e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 47,
                "offset": 3,
                "shift": 21,
                "w": 17,
                "x": 485,
                "y": 100
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "f7417eb9-9a00-4ec7-a2b6-977a66f5c1bd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 47,
                "offset": 3,
                "shift": 34,
                "w": 28,
                "x": 167,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "d4b8f1d6-b9f0-417a-8a28-cd38a6a1cb1b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 47,
                "offset": 3,
                "shift": 28,
                "w": 23,
                "x": 469,
                "y": 2
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "a1dc77fc-9b49-4e66-9e1b-1104969b69d9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 47,
                "offset": 2,
                "shift": 30,
                "w": 26,
                "x": 225,
                "y": 2
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "1864a6a8-3a54-435d-8780-d15de370560f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 47,
                "offset": 3,
                "shift": 23,
                "w": 19,
                "x": 441,
                "y": 51
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "d39e071f-16f4-4c5a-8369-1e28819c8c54",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 47,
                "offset": 2,
                "shift": 30,
                "w": 26,
                "x": 197,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "cf714b44-f857-40e3-9910-a5d787ace24f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 47,
                "offset": 3,
                "shift": 24,
                "w": 21,
                "x": 309,
                "y": 51
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "5025d081-09f9-47ae-8612-a80ce23adb01",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 47,
                "offset": 1,
                "shift": 23,
                "w": 21,
                "x": 286,
                "y": 51
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "2589ecb3-5e49-453c-a741-3aa2ef4fde02",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 47,
                "offset": 1,
                "shift": 23,
                "w": 21,
                "x": 263,
                "y": 51
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "eae24149-6674-4b9c-80e3-4930176533d0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 47,
                "offset": 3,
                "shift": 28,
                "w": 23,
                "x": 52,
                "y": 51
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "d172012d-f597-4e83-b172-945c7046edb6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 47,
                "offset": 0,
                "shift": 25,
                "w": 25,
                "x": 335,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "6e85247d-fc42-4a77-b4e6-10573bf099e4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 47,
                "offset": 0,
                "shift": 37,
                "w": 37,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "fe12d61a-dcd1-4f85-a425-3748fe052358",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 47,
                "offset": 0,
                "shift": 25,
                "w": 25,
                "x": 308,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "4f5d4673-3be9-40f4-aa7e-676e46d791ed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 47,
                "offset": 0,
                "shift": 24,
                "w": 24,
                "x": 443,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "578cb048-2eed-47ab-b705-35b242104075",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 47,
                "offset": 2,
                "shift": 25,
                "w": 21,
                "x": 171,
                "y": 51
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "8d070022-7004-4e5b-be8d-3ca85ad204e7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 47,
                "offset": 4,
                "shift": 13,
                "w": 9,
                "x": 228,
                "y": 149
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "23fb49c9-2ec5-4b11-8f86-2243c0eb5d05",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 47,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 114,
                "y": 149
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "9f3b62a4-ae86-45d2-a901-183466cf132b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 47,
                "offset": 1,
                "shift": 14,
                "w": 9,
                "x": 217,
                "y": 149
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "ef75993b-e8d1-4aa7-accc-7e528bfcf3d6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 47,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 53,
                "y": 149
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "8ae2b7e9-e668-4f67-ab97-12694dde6945",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 47,
                "offset": -1,
                "shift": 16,
                "w": 18,
                "x": 273,
                "y": 100
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "5d6c1386-d912-4b56-a508-df79825ee2c8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 47,
                "offset": 5,
                "shift": 20,
                "w": 9,
                "x": 206,
                "y": 149
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "c205edec-4d40-4bbc-9f02-6dffc456e64a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 47,
                "offset": 1,
                "shift": 20,
                "w": 17,
                "x": 447,
                "y": 100
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "689124dd-d5ca-41e3-a348-24beacb190ba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 47,
                "offset": 2,
                "shift": 22,
                "w": 19,
                "x": 2,
                "y": 100
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "1ea68046-80ab-4f91-8501-03977db4a770",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 47,
                "offset": 1,
                "shift": 20,
                "w": 18,
                "x": 253,
                "y": 100
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "49717e48-c31d-4916-b6d1-253c80951455",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 47,
                "offset": 1,
                "shift": 22,
                "w": 19,
                "x": 462,
                "y": 51
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "10f2353e-5d8b-4a70-924d-00ad24ac0af3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 47,
                "offset": 1,
                "shift": 20,
                "w": 18,
                "x": 233,
                "y": 100
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "614f1556-766c-4891-92ae-d3729322dd0a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 47,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 170,
                "y": 149
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "6929057f-096c-42ca-8b2d-4a57acb5152f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 47,
                "offset": 1,
                "shift": 22,
                "w": 19,
                "x": 44,
                "y": 100
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "21518222-e227-4335-8709-708aa5b8efdf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 47,
                "offset": 2,
                "shift": 21,
                "w": 17,
                "x": 466,
                "y": 100
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "87541a00-51d0-47bb-b6e6-d743bd33c854",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 47,
                "offset": 2,
                "shift": 8,
                "w": 4,
                "x": 339,
                "y": 149
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "0578be4e-956b-42d6-9cc0-4b7b1b8a215a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 47,
                "offset": -1,
                "shift": 8,
                "w": 7,
                "x": 280,
                "y": 149
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "e9922be0-7396-497f-b1a0-3612847498b8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 47,
                "offset": 2,
                "shift": 19,
                "w": 17,
                "x": 409,
                "y": 100
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "62862b69-54a8-440d-a29e-d703721904a9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 47,
                "offset": 2,
                "shift": 8,
                "w": 4,
                "x": 303,
                "y": 149
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "295a4622-5fe5-4341-88ee-2e6e9a43813c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 47,
                "offset": 2,
                "shift": 32,
                "w": 28,
                "x": 137,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "f5bf8dab-887e-4ab1-bfcc-7f1f537e0d16",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 47,
                "offset": 2,
                "shift": 21,
                "w": 17,
                "x": 390,
                "y": 100
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "e7ffc5e0-caab-4741-888b-c48c3b50cf3d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 47,
                "offset": 1,
                "shift": 21,
                "w": 19,
                "x": 23,
                "y": 100
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "b1de7a37-5715-44b0-838a-928a6b0f2d8e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 47,
                "offset": 2,
                "shift": 22,
                "w": 19,
                "x": 65,
                "y": 100
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "89276a15-c7e8-4f2b-b25f-86c5ed69d72a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 47,
                "offset": 1,
                "shift": 22,
                "w": 19,
                "x": 86,
                "y": 100
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "55f41a51-5716-47a8-9fd1-6247b79298a8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 47,
                "offset": 2,
                "shift": 12,
                "w": 10,
                "x": 194,
                "y": 149
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "a859895e-acd2-4707-a8aa-e9dbd9a60d0c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 47,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 333,
                "y": 100
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "fba5c5a5-4d01-4b0c-8037-0a55afe8928e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 47,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 182,
                "y": 149
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "452abd2f-3f66-4ec3-8e3a-d80a87ac80dc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 47,
                "offset": 2,
                "shift": 21,
                "w": 17,
                "x": 371,
                "y": 100
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "b52b8e53-e543-4cda-8729-a5c843d27130",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 47,
                "offset": 0,
                "shift": 19,
                "w": 19,
                "x": 107,
                "y": 100
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "4b864d16-8d3e-4caf-b1e6-124aec85b676",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 47,
                "offset": 0,
                "shift": 29,
                "w": 29,
                "x": 75,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "2155958f-9599-4fbe-a4c1-1640b4b73b1a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 47,
                "offset": 0,
                "shift": 19,
                "w": 19,
                "x": 128,
                "y": 100
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "19fdfd00-3b8b-4160-a3a8-4b6bfb206f39",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 47,
                "offset": 0,
                "shift": 19,
                "w": 19,
                "x": 170,
                "y": 100
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "eb21c997-1740-45ef-9018-ef19a3625dbb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 47,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 20,
                "y": 149
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "5d31b5af-254c-43b4-bc9c-b63d2016cc23",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 47,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 143,
                "y": 149
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "88010810-bd78-45cf-b297-236dc5659e2e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 47,
                "offset": 5,
                "shift": 13,
                "w": 4,
                "x": 327,
                "y": 149
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "873c1e1d-314a-4009-a5ce-3a241ed189f9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 47,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 84,
                "y": 149
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "b944dd64-887e-4c5f-8583-c67021c0cdcc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 47,
                "offset": 2,
                "shift": 24,
                "w": 20,
                "x": 354,
                "y": 51
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 30,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}