{
    "id": "454c5d23-6b18-4eb0-8334-1ebb6167dce5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "exit_wall",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "05072bd8-6e0f-44d7-97b4-660a04eaaec3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "454c5d23-6b18-4eb0-8334-1ebb6167dce5",
            "compositeImage": {
                "id": "ce0b5694-c3c8-4dfa-a936-c1d2cb854136",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "05072bd8-6e0f-44d7-97b4-660a04eaaec3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0f7329c9-3513-40cd-b9c0-061815d32420",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "05072bd8-6e0f-44d7-97b4-660a04eaaec3",
                    "LayerId": "565b85e4-96fd-4d9d-a266-b2cc6668c5f4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "565b85e4-96fd-4d9d-a266-b2cc6668c5f4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "454c5d23-6b18-4eb0-8334-1ebb6167dce5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}