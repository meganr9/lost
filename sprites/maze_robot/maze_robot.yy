{
    "id": "39d62d34-8030-400d-9fce-1e8d66a0451d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "maze_robot",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 48,
    "bbox_left": 16,
    "bbox_right": 116,
    "bbox_top": 18,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e47fc81a-b698-4c74-b3e7-8e8ad054e725",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "39d62d34-8030-400d-9fce-1e8d66a0451d",
            "compositeImage": {
                "id": "2ef7c68e-77ce-4feb-a169-7f1a72c4ef36",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e47fc81a-b698-4c74-b3e7-8e8ad054e725",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "687a7f6e-cf59-4721-83a0-ad3576dc8d09",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e47fc81a-b698-4c74-b3e7-8e8ad054e725",
                    "LayerId": "52946a09-e765-43f6-8efc-5bed6dc133c3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "52946a09-e765-43f6-8efc-5bed6dc133c3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "39d62d34-8030-400d-9fce-1e8d66a0451d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 32
}