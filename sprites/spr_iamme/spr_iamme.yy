{
    "id": "ec518722-cf0d-4867-bf7f-c8d516341989",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_iamme",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 479,
    "bbox_left": 0,
    "bbox_right": 639,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "8740c993-deda-47bf-b3f8-d20712cb4dcf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ec518722-cf0d-4867-bf7f-c8d516341989",
            "compositeImage": {
                "id": "47112031-19f8-4efe-86e5-8dde56b652ad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8740c993-deda-47bf-b3f8-d20712cb4dcf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bd7534e5-8739-48cd-aa14-0d7808b18884",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8740c993-deda-47bf-b3f8-d20712cb4dcf",
                    "LayerId": "93ea21c5-06e5-4495-901f-c0a78a747c74"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 480,
    "layers": [
        {
            "id": "93ea21c5-06e5-4495-901f-c0a78a747c74",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ec518722-cf0d-4867-bf7f-c8d516341989",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 640,
    "xorig": -228,
    "yorig": -33
}