{
    "id": "31619ff7-9882-4fc4-9666-25760d6ef545",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "boss",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 16,
    "bbox_right": 46,
    "bbox_top": 17,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "49ae16de-9943-4406-bfff-8342ef13ff40",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "31619ff7-9882-4fc4-9666-25760d6ef545",
            "compositeImage": {
                "id": "c4a8f6ac-537b-42cd-b340-aab9554f6633",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "49ae16de-9943-4406-bfff-8342ef13ff40",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "acc3fb50-8854-450c-af40-0b65b63e12bc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "49ae16de-9943-4406-bfff-8342ef13ff40",
                    "LayerId": "a4f44cc0-49e6-4323-8e0f-287c33dcd7bf"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "a4f44cc0-49e6-4323-8e0f-287c33dcd7bf",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "31619ff7-9882-4fc4-9666-25760d6ef545",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}