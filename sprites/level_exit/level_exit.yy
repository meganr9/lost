{
    "id": "0aa8844b-46f8-49c2-81a1-5adbfe9e3191",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "level_exit",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0e9e4382-1d5e-438d-b4cd-783295066130",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0aa8844b-46f8-49c2-81a1-5adbfe9e3191",
            "compositeImage": {
                "id": "83d0397f-4f07-4b9e-9e7a-65025e2bb526",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0e9e4382-1d5e-438d-b4cd-783295066130",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f824b3f3-60b9-4c87-bdcc-3a681acc6420",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0e9e4382-1d5e-438d-b4cd-783295066130",
                    "LayerId": "13f11acd-5b55-4cb1-9441-877aeb7ce3ef"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "13f11acd-5b55-4cb1-9441-877aeb7ce3ef",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0aa8844b-46f8-49c2-81a1-5adbfe9e3191",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}