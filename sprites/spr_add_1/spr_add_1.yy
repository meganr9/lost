{
    "id": "c4d7406f-0fb6-40b9-b680-92bfaaf7b69d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_add_1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 478,
    "bbox_left": 98,
    "bbox_right": 565,
    "bbox_top": 16,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "3fa39e11-bd67-4056-9185-bbe46abff1a3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c4d7406f-0fb6-40b9-b680-92bfaaf7b69d",
            "compositeImage": {
                "id": "042aa991-8f41-49fe-887d-6c6365908d10",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3fa39e11-bd67-4056-9185-bbe46abff1a3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b96b3379-f961-4125-97b5-c280ac79c093",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3fa39e11-bd67-4056-9185-bbe46abff1a3",
                    "LayerId": "2d987bd9-1ad4-4d3f-9307-facfb6bdd02e"
                }
            ]
        },
        {
            "id": "06fd191a-ad28-4527-b16f-7ab7b1be00b0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c4d7406f-0fb6-40b9-b680-92bfaaf7b69d",
            "compositeImage": {
                "id": "c63ce59f-e8be-4261-9d54-36091a4a3044",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "06fd191a-ad28-4527-b16f-7ab7b1be00b0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b3eba678-0e1f-40d0-beeb-4d0f40b3552e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "06fd191a-ad28-4527-b16f-7ab7b1be00b0",
                    "LayerId": "2d987bd9-1ad4-4d3f-9307-facfb6bdd02e"
                }
            ]
        },
        {
            "id": "1362d08e-646b-4184-aab8-249f1e1155a9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c4d7406f-0fb6-40b9-b680-92bfaaf7b69d",
            "compositeImage": {
                "id": "67ad03b1-0c46-4ce0-b66b-1c0638b90076",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1362d08e-646b-4184-aab8-249f1e1155a9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fbfe5411-13cb-4b14-a1f7-dbb4e5bd2c70",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1362d08e-646b-4184-aab8-249f1e1155a9",
                    "LayerId": "2d987bd9-1ad4-4d3f-9307-facfb6bdd02e"
                }
            ]
        },
        {
            "id": "b7a2fde7-17a7-422b-af76-c7b370c6e982",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c4d7406f-0fb6-40b9-b680-92bfaaf7b69d",
            "compositeImage": {
                "id": "60f7b41a-9458-4948-bcbd-4deee35938a3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b7a2fde7-17a7-422b-af76-c7b370c6e982",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6e5df244-f585-4977-a0d8-1f535858eac6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b7a2fde7-17a7-422b-af76-c7b370c6e982",
                    "LayerId": "2d987bd9-1ad4-4d3f-9307-facfb6bdd02e"
                }
            ]
        },
        {
            "id": "b71bb15d-c18c-48d6-91b9-4abf90f9620f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c4d7406f-0fb6-40b9-b680-92bfaaf7b69d",
            "compositeImage": {
                "id": "a5fdfdef-4f10-4a42-bd5a-0c0d33f53897",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b71bb15d-c18c-48d6-91b9-4abf90f9620f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6de1955b-8a4e-40f7-8608-1a1355ae9798",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b71bb15d-c18c-48d6-91b9-4abf90f9620f",
                    "LayerId": "2d987bd9-1ad4-4d3f-9307-facfb6bdd02e"
                }
            ]
        },
        {
            "id": "4718c95e-710e-4b78-910d-88ccc804a6e2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c4d7406f-0fb6-40b9-b680-92bfaaf7b69d",
            "compositeImage": {
                "id": "a14647c3-d388-46f6-bd5c-0d77da515a8b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4718c95e-710e-4b78-910d-88ccc804a6e2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "28617e4f-e96a-45c4-9530-787a73fb6424",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4718c95e-710e-4b78-910d-88ccc804a6e2",
                    "LayerId": "2d987bd9-1ad4-4d3f-9307-facfb6bdd02e"
                }
            ]
        },
        {
            "id": "5835fd8e-77e1-4c4c-aa59-385926fdbd27",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c4d7406f-0fb6-40b9-b680-92bfaaf7b69d",
            "compositeImage": {
                "id": "a3a41d03-5170-41f5-9b30-ad9b3f79903f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5835fd8e-77e1-4c4c-aa59-385926fdbd27",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c801a1e2-f088-4813-a37e-57d882fe1883",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5835fd8e-77e1-4c4c-aa59-385926fdbd27",
                    "LayerId": "2d987bd9-1ad4-4d3f-9307-facfb6bdd02e"
                }
            ]
        },
        {
            "id": "51764e4b-29bc-47bb-90be-a0ef3e4fbc7b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c4d7406f-0fb6-40b9-b680-92bfaaf7b69d",
            "compositeImage": {
                "id": "0ac44fd1-f54c-46fd-82e6-7d5a16b2d9f2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "51764e4b-29bc-47bb-90be-a0ef3e4fbc7b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c2f97994-ab1d-4887-afb9-ebaf804fe34b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "51764e4b-29bc-47bb-90be-a0ef3e4fbc7b",
                    "LayerId": "2d987bd9-1ad4-4d3f-9307-facfb6bdd02e"
                }
            ]
        },
        {
            "id": "9afc3910-d7d9-4666-8d5e-9cf8f48aa202",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c4d7406f-0fb6-40b9-b680-92bfaaf7b69d",
            "compositeImage": {
                "id": "03c96c0d-005b-459c-b3a4-a538d3ce05a6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9afc3910-d7d9-4666-8d5e-9cf8f48aa202",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "724061ae-6a00-4ee0-a0e4-c404016b639f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9afc3910-d7d9-4666-8d5e-9cf8f48aa202",
                    "LayerId": "2d987bd9-1ad4-4d3f-9307-facfb6bdd02e"
                }
            ]
        },
        {
            "id": "ed962f6c-3e1e-43cb-a856-05af162eceb7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c4d7406f-0fb6-40b9-b680-92bfaaf7b69d",
            "compositeImage": {
                "id": "9d5b69a0-7ed8-4791-9f53-555ff7ddd33a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ed962f6c-3e1e-43cb-a856-05af162eceb7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0a8d53dc-75ce-42da-81c8-f322efb12bb2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ed962f6c-3e1e-43cb-a856-05af162eceb7",
                    "LayerId": "2d987bd9-1ad4-4d3f-9307-facfb6bdd02e"
                }
            ]
        },
        {
            "id": "2f434f80-17e9-4605-9a0b-d81dfb563265",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c4d7406f-0fb6-40b9-b680-92bfaaf7b69d",
            "compositeImage": {
                "id": "bfeb1daf-dd3e-4670-9218-56d8f5c8297c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2f434f80-17e9-4605-9a0b-d81dfb563265",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e26baf32-22d5-424e-8bda-f4552a4928fa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2f434f80-17e9-4605-9a0b-d81dfb563265",
                    "LayerId": "2d987bd9-1ad4-4d3f-9307-facfb6bdd02e"
                }
            ]
        },
        {
            "id": "4b87e96e-130a-44ae-8438-a1803f67168c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c4d7406f-0fb6-40b9-b680-92bfaaf7b69d",
            "compositeImage": {
                "id": "b4a5f4c4-2317-418a-b6f1-3eea597949ba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4b87e96e-130a-44ae-8438-a1803f67168c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "75c377d9-bda6-44a2-b876-c56ed57a174e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4b87e96e-130a-44ae-8438-a1803f67168c",
                    "LayerId": "2d987bd9-1ad4-4d3f-9307-facfb6bdd02e"
                }
            ]
        },
        {
            "id": "c40e9d2a-40eb-49d6-97b3-48561ee63d48",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c4d7406f-0fb6-40b9-b680-92bfaaf7b69d",
            "compositeImage": {
                "id": "4264b358-a1de-4492-97a4-1b5617c2cd40",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c40e9d2a-40eb-49d6-97b3-48561ee63d48",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b1415a3b-5046-4906-af64-1eebdaa7e870",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c40e9d2a-40eb-49d6-97b3-48561ee63d48",
                    "LayerId": "2d987bd9-1ad4-4d3f-9307-facfb6bdd02e"
                }
            ]
        },
        {
            "id": "603836fc-8068-4269-a93e-9b42c3a43d65",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c4d7406f-0fb6-40b9-b680-92bfaaf7b69d",
            "compositeImage": {
                "id": "e7acb3a4-2bdc-4427-b304-be55ad27180a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "603836fc-8068-4269-a93e-9b42c3a43d65",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f8b26a8b-b6b5-4130-a123-0979be0480e0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "603836fc-8068-4269-a93e-9b42c3a43d65",
                    "LayerId": "2d987bd9-1ad4-4d3f-9307-facfb6bdd02e"
                }
            ]
        },
        {
            "id": "7b85d2cc-506e-43ee-8cde-d79a290d2fd0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c4d7406f-0fb6-40b9-b680-92bfaaf7b69d",
            "compositeImage": {
                "id": "607920b1-d544-4777-88ab-ee61eba1ea4e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7b85d2cc-506e-43ee-8cde-d79a290d2fd0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aa6483ec-4aea-40d5-aaf7-defa902e7fa0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7b85d2cc-506e-43ee-8cde-d79a290d2fd0",
                    "LayerId": "2d987bd9-1ad4-4d3f-9307-facfb6bdd02e"
                }
            ]
        },
        {
            "id": "2e5051b9-88d0-4dc0-9db2-c1c0dab9060a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c4d7406f-0fb6-40b9-b680-92bfaaf7b69d",
            "compositeImage": {
                "id": "fb8ab617-fced-4a70-b5d0-17e3f9058a67",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2e5051b9-88d0-4dc0-9db2-c1c0dab9060a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0a447c83-f619-4552-8f19-901fbe4fd988",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2e5051b9-88d0-4dc0-9db2-c1c0dab9060a",
                    "LayerId": "2d987bd9-1ad4-4d3f-9307-facfb6bdd02e"
                }
            ]
        },
        {
            "id": "6d2eeecb-f386-47fd-a23e-51a289f1b9b3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c4d7406f-0fb6-40b9-b680-92bfaaf7b69d",
            "compositeImage": {
                "id": "f64f55fa-b3a4-4496-b5fe-a17aa390fe21",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6d2eeecb-f386-47fd-a23e-51a289f1b9b3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "97d04a27-c727-4ea9-9e76-ab1418054585",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6d2eeecb-f386-47fd-a23e-51a289f1b9b3",
                    "LayerId": "2d987bd9-1ad4-4d3f-9307-facfb6bdd02e"
                }
            ]
        },
        {
            "id": "be24cd79-5fc0-46b7-a7cb-6a4217f80f3a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c4d7406f-0fb6-40b9-b680-92bfaaf7b69d",
            "compositeImage": {
                "id": "3b7f654e-c9d7-44bd-bcc6-a67277595920",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "be24cd79-5fc0-46b7-a7cb-6a4217f80f3a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e465b087-4249-44bb-a3ac-9a792bad06a5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "be24cd79-5fc0-46b7-a7cb-6a4217f80f3a",
                    "LayerId": "2d987bd9-1ad4-4d3f-9307-facfb6bdd02e"
                }
            ]
        },
        {
            "id": "12bb9058-b807-4bd3-beab-c8bf3eeafeeb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c4d7406f-0fb6-40b9-b680-92bfaaf7b69d",
            "compositeImage": {
                "id": "f401d6a8-7210-409b-b7a0-b613f8569e05",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "12bb9058-b807-4bd3-beab-c8bf3eeafeeb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9227e56e-84e7-4826-b2a4-cd5bc14b0348",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "12bb9058-b807-4bd3-beab-c8bf3eeafeeb",
                    "LayerId": "2d987bd9-1ad4-4d3f-9307-facfb6bdd02e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 480,
    "layers": [
        {
            "id": "2d987bd9-1ad4-4d3f-9307-facfb6bdd02e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c4d7406f-0fb6-40b9-b680-92bfaaf7b69d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 640,
    "xorig": 320,
    "yorig": 240
}