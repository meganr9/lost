{
    "id": "1852803d-1226-4987-8c31-6ae20660782d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_previous_rm",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "449c7e6e-430f-4481-8cf5-1187168cf57b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1852803d-1226-4987-8c31-6ae20660782d",
            "compositeImage": {
                "id": "b884b5dd-a866-4ceb-90ca-62c3d507a8dd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "449c7e6e-430f-4481-8cf5-1187168cf57b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "566f7b29-efd6-4ec6-aa71-3249aa6be703",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "449c7e6e-430f-4481-8cf5-1187168cf57b",
                    "LayerId": "c3217bb7-57a9-4f23-997b-5c5c10c68644"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "c3217bb7-57a9-4f23-997b-5c5c10c68644",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1852803d-1226-4987-8c31-6ae20660782d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}