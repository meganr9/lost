{
    "id": "5b8a507d-f5fa-4102-a4fb-80e83ab40ce0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_roomc",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "5996d3d7-cd64-49a8-9353-da23ef66e291",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5b8a507d-f5fa-4102-a4fb-80e83ab40ce0",
            "compositeImage": {
                "id": "ef14bff9-5bc9-4cd4-a175-cedd3c1fddc8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5996d3d7-cd64-49a8-9353-da23ef66e291",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ead36398-3ad7-45a4-9dba-4bff72296fe7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5996d3d7-cd64-49a8-9353-da23ef66e291",
                    "LayerId": "12077786-6ccd-46c3-b80a-273694e09eb4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "12077786-6ccd-46c3-b80a-273694e09eb4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5b8a507d-f5fa-4102-a4fb-80e83ab40ce0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}