{
    "id": "a2095d69-0622-4249-8054-651f719ab2c8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_dialog_background",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f4343b85-5d3d-4f3a-81f3-db340aa12bf5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a2095d69-0622-4249-8054-651f719ab2c8",
            "compositeImage": {
                "id": "fcab8d49-698d-4d3f-8d3c-1511115a3430",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f4343b85-5d3d-4f3a-81f3-db340aa12bf5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8037dca5-1abd-4a45-b846-702a0cd81016",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f4343b85-5d3d-4f3a-81f3-db340aa12bf5",
                    "LayerId": "48574ef9-2cbd-4a3d-bfaa-ce6cc34d1965"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "48574ef9-2cbd-4a3d-bfaa-ce6cc34d1965",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a2095d69-0622-4249-8054-651f719ab2c8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}