{
    "id": "c4d388d2-eb3f-4b44-8e80-c4c14dbd9a7d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "three_way_conector",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7915ba6a-d021-4811-9788-37f5946dbda5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c4d388d2-eb3f-4b44-8e80-c4c14dbd9a7d",
            "compositeImage": {
                "id": "48d6278b-7fe1-4044-8005-4ee4b4542f40",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7915ba6a-d021-4811-9788-37f5946dbda5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e93f1248-2942-4778-9e75-40eb61eeb9bf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7915ba6a-d021-4811-9788-37f5946dbda5",
                    "LayerId": "c29ce484-cc14-4b91-aa92-6f6cb69ae638"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "c29ce484-cc14-4b91-aa92-6f6cb69ae638",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c4d388d2-eb3f-4b44-8e80-c4c14dbd9a7d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}