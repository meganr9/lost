{
    "id": "565dcac8-1f61-4a50-addf-06c8368973c4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player_up",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 113,
    "bbox_left": 258,
    "bbox_right": 338,
    "bbox_top": 42,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "416467ef-8c21-4794-96e5-6b4f81de255e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "565dcac8-1f61-4a50-addf-06c8368973c4",
            "compositeImage": {
                "id": "ce05bf5d-1468-4dff-9c51-8043fae6ec6b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "416467ef-8c21-4794-96e5-6b4f81de255e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5006f15a-cda8-4b4b-934a-591cc36fdb8f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "416467ef-8c21-4794-96e5-6b4f81de255e",
                    "LayerId": "548470cc-17ef-40a9-9ca9-1c131dad2665"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 480,
    "layers": [
        {
            "id": "548470cc-17ef-40a9-9ca9-1c131dad2665",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "565dcac8-1f61-4a50-addf-06c8368973c4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 640,
    "xorig": 0,
    "yorig": 0
}