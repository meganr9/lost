{
    "id": "295dccb6-6db1-47a7-bca7-150c7b362da0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite38",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 166,
    "bbox_left": 92,
    "bbox_right": 253,
    "bbox_top": 36,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "56e72061-6465-467c-8f76-a6ce7d838810",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "295dccb6-6db1-47a7-bca7-150c7b362da0",
            "compositeImage": {
                "id": "59d7157e-f68d-43fc-a833-ec911b426075",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "56e72061-6465-467c-8f76-a6ce7d838810",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "71041abc-e1f6-4e10-b60c-6a7df8364227",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "56e72061-6465-467c-8f76-a6ce7d838810",
                    "LayerId": "454e6059-ea3b-4f92-9bf3-732dda58944f"
                }
            ]
        },
        {
            "id": "00ebc3a9-dfed-4647-9019-31ec6198bfbe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "295dccb6-6db1-47a7-bca7-150c7b362da0",
            "compositeImage": {
                "id": "941e4d69-c5e8-44d1-a9bd-39da206773fc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "00ebc3a9-dfed-4647-9019-31ec6198bfbe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9e41111d-4f5c-4cf6-aaf4-f1b5f7b9bd70",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "00ebc3a9-dfed-4647-9019-31ec6198bfbe",
                    "LayerId": "454e6059-ea3b-4f92-9bf3-732dda58944f"
                }
            ]
        },
        {
            "id": "bf3c0785-b94a-42fb-8d49-d114c4f4f9d0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "295dccb6-6db1-47a7-bca7-150c7b362da0",
            "compositeImage": {
                "id": "a4c2f64a-15d6-44c9-a064-d1b7b52185bc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bf3c0785-b94a-42fb-8d49-d114c4f4f9d0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e0e08443-4ac9-41ae-811f-48875910d341",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bf3c0785-b94a-42fb-8d49-d114c4f4f9d0",
                    "LayerId": "454e6059-ea3b-4f92-9bf3-732dda58944f"
                }
            ]
        },
        {
            "id": "75d42f91-5402-468f-bdb2-3c32511cb909",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "295dccb6-6db1-47a7-bca7-150c7b362da0",
            "compositeImage": {
                "id": "17d38a87-d538-4b66-9e0e-cf0fef9d4cbe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "75d42f91-5402-468f-bdb2-3c32511cb909",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5cfda11d-76c2-44dc-9715-215d0ca18058",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "75d42f91-5402-468f-bdb2-3c32511cb909",
                    "LayerId": "454e6059-ea3b-4f92-9bf3-732dda58944f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 240,
    "layers": [
        {
            "id": "454e6059-ea3b-4f92-9bf3-732dda58944f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "295dccb6-6db1-47a7-bca7-150c7b362da0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 320,
    "xorig": -127,
    "yorig": 55
}