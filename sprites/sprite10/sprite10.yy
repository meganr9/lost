{
    "id": "9dd66608-3573-4a55-bcb7-56f9738bcab8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite10",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 479,
    "bbox_left": 0,
    "bbox_right": 639,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "d7f93b5f-70f7-4064-a1dc-f86e2bb0f677",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9dd66608-3573-4a55-bcb7-56f9738bcab8",
            "compositeImage": {
                "id": "b8de9c7f-044b-49e7-90ab-65b2b5d7445b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d7f93b5f-70f7-4064-a1dc-f86e2bb0f677",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d19fa809-a8df-4140-8f2b-1ed85a83eb2c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d7f93b5f-70f7-4064-a1dc-f86e2bb0f677",
                    "LayerId": "82137c68-868f-4818-bc43-d641bb4ead86"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 480,
    "layers": [
        {
            "id": "82137c68-868f-4818-bc43-d641bb4ead86",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9dd66608-3573-4a55-bcb7-56f9738bcab8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 640,
    "xorig": 0,
    "yorig": 0
}