{
    "id": "520bdc18-0244-4bfc-ae4b-90ba4e62e463",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "one_way_conector",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "23d09c96-fd8a-4732-9e60-80e0a146b06e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "520bdc18-0244-4bfc-ae4b-90ba4e62e463",
            "compositeImage": {
                "id": "f098ec15-7b74-4d37-84b6-43f39c377600",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "23d09c96-fd8a-4732-9e60-80e0a146b06e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c87056c1-06ae-4a14-a297-3234c1a0c6d4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "23d09c96-fd8a-4732-9e60-80e0a146b06e",
                    "LayerId": "a37cfc04-20e5-4c82-af8b-be7ff1106ad7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "a37cfc04-20e5-4c82-af8b-be7ff1106ad7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "520bdc18-0244-4bfc-ae4b-90ba4e62e463",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}