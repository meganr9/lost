{
    "id": "b9866967-1d07-49c4-b272-1ac97867aa97",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_hand_part",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 406,
    "bbox_left": 306,
    "bbox_right": 348,
    "bbox_top": 375,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "9151eb35-6253-4c48-8dbf-db3a4ce5235e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b9866967-1d07-49c4-b272-1ac97867aa97",
            "compositeImage": {
                "id": "29a89729-5922-4608-b8e1-5bf21f9326f6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9151eb35-6253-4c48-8dbf-db3a4ce5235e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "94b3bf8c-3fba-43e7-968a-37ef8adca45f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9151eb35-6253-4c48-8dbf-db3a4ce5235e",
                    "LayerId": "0746e3f2-ed08-4256-96da-a570b27f305f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 480,
    "layers": [
        {
            "id": "0746e3f2-ed08-4256-96da-a570b27f305f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b9866967-1d07-49c4-b272-1ac97867aa97",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 640,
    "xorig": 0,
    "yorig": 0
}