{
    "id": "6ff40d3f-9000-4526-9fe1-32f736e583f4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "corner_way_conector",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e20dc6fe-9784-493e-88e1-45ad6ba82930",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6ff40d3f-9000-4526-9fe1-32f736e583f4",
            "compositeImage": {
                "id": "0dfeb564-db5d-425d-a721-c99d042c417c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e20dc6fe-9784-493e-88e1-45ad6ba82930",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0f78a244-59d2-4367-8ad5-a8fdacf10c71",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e20dc6fe-9784-493e-88e1-45ad6ba82930",
                    "LayerId": "5d9ddb6c-01b0-4a49-952f-0c69666ab38a"
                }
            ]
        },
        {
            "id": "5035a578-7e73-4d01-91fd-6a7a0c9efc21",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6ff40d3f-9000-4526-9fe1-32f736e583f4",
            "compositeImage": {
                "id": "74253dca-eea1-4dda-8f4e-b2e0f0a00c86",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5035a578-7e73-4d01-91fd-6a7a0c9efc21",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "73a5f73e-09b1-4e0f-9e9a-8aa64eac9ea7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5035a578-7e73-4d01-91fd-6a7a0c9efc21",
                    "LayerId": "5d9ddb6c-01b0-4a49-952f-0c69666ab38a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "5d9ddb6c-01b0-4a49-952f-0c69666ab38a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6ff40d3f-9000-4526-9fe1-32f736e583f4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}