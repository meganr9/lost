{
    "id": "7e3880ed-fe6b-4453-b3c5-a53e39bebc41",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_tileset1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 767,
    "bbox_left": 0,
    "bbox_right": 1023,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "9973d42c-ffd1-45c7-88d2-cb732a28ed3e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7e3880ed-fe6b-4453-b3c5-a53e39bebc41",
            "compositeImage": {
                "id": "bd178c7c-0455-44b1-b131-62d25ea7dc81",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9973d42c-ffd1-45c7-88d2-cb732a28ed3e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f3e6f028-e60d-4680-b228-90327bf13c30",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9973d42c-ffd1-45c7-88d2-cb732a28ed3e",
                    "LayerId": "27a43dec-0f0d-4713-8725-9a5f674cf20e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 768,
    "layers": [
        {
            "id": "27a43dec-0f0d-4713-8725-9a5f674cf20e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7e3880ed-fe6b-4453-b3c5-a53e39bebc41",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1024,
    "xorig": 0,
    "yorig": 0
}