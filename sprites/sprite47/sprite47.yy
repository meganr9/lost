{
    "id": "98cc4c6f-fb2b-4689-9b75-669a25ea8511",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite47",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 471,
    "bbox_left": 164,
    "bbox_right": 490,
    "bbox_top": 43,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "7d143dd5-5945-46a0-9a6f-35816581f0a7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "98cc4c6f-fb2b-4689-9b75-669a25ea8511",
            "compositeImage": {
                "id": "87bb6728-f309-4196-8601-1b01f34fa54a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7d143dd5-5945-46a0-9a6f-35816581f0a7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "992f5b7f-64cd-45ea-aac2-7bed878a59ab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7d143dd5-5945-46a0-9a6f-35816581f0a7",
                    "LayerId": "c09bad6c-202d-44f6-8c8a-5ad30752493a"
                }
            ]
        },
        {
            "id": "0a8997f1-619f-4db5-9d20-3966bcf5a337",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "98cc4c6f-fb2b-4689-9b75-669a25ea8511",
            "compositeImage": {
                "id": "f42be29b-61ac-4380-a928-57a4a1e31ce4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0a8997f1-619f-4db5-9d20-3966bcf5a337",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fbd93c81-0ed7-4422-b5a2-9f76c81bf21f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0a8997f1-619f-4db5-9d20-3966bcf5a337",
                    "LayerId": "c09bad6c-202d-44f6-8c8a-5ad30752493a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 480,
    "layers": [
        {
            "id": "c09bad6c-202d-44f6-8c8a-5ad30752493a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "98cc4c6f-fb2b-4689-9b75-669a25ea8511",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 640,
    "xorig": 0,
    "yorig": 0
}