{
    "id": "662ea7b2-4a3f-4fcc-a723-d40d0182cc62",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_moving_forward_1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 478,
    "bbox_left": 221,
    "bbox_right": 393,
    "bbox_top": 45,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "71ff743b-8b84-4f84-86b0-3c5543df5304",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "662ea7b2-4a3f-4fcc-a723-d40d0182cc62",
            "compositeImage": {
                "id": "4e2d33fe-edf0-4d9c-a8d8-e49333720bdd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "71ff743b-8b84-4f84-86b0-3c5543df5304",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4c765640-7ec4-4d3a-be96-e62e4e7928fc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "71ff743b-8b84-4f84-86b0-3c5543df5304",
                    "LayerId": "8e65a008-80b2-4361-9c06-db8594d5dc10"
                }
            ]
        },
        {
            "id": "f7c716db-b18f-44d2-ba79-e342bed553b7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "662ea7b2-4a3f-4fcc-a723-d40d0182cc62",
            "compositeImage": {
                "id": "743b6d3f-051c-4e45-93b9-b21992f22d4a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f7c716db-b18f-44d2-ba79-e342bed553b7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "40ca2f63-debf-4904-b908-6ea4ae9ea646",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f7c716db-b18f-44d2-ba79-e342bed553b7",
                    "LayerId": "8e65a008-80b2-4361-9c06-db8594d5dc10"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 480,
    "layers": [
        {
            "id": "8e65a008-80b2-4361-9c06-db8594d5dc10",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "662ea7b2-4a3f-4fcc-a723-d40d0182cc62",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 640,
    "xorig": 0,
    "yorig": 0
}