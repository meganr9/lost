{
    "id": "85f44437-8964-43a8-923e-8fead1f49b69",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "three_way_conector_live",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f5119a4c-4a59-4205-99cc-fc9c059c8a66",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "85f44437-8964-43a8-923e-8fead1f49b69",
            "compositeImage": {
                "id": "3a438dec-870e-47da-9df0-73b3993b64b7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f5119a4c-4a59-4205-99cc-fc9c059c8a66",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3997e7da-d0ee-41a1-962d-2f123445642b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f5119a4c-4a59-4205-99cc-fc9c059c8a66",
                    "LayerId": "9205cb7d-75ab-4f76-90ee-640d8a9a0c82"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "9205cb7d-75ab-4f76-90ee-640d8a9a0c82",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "85f44437-8964-43a8-923e-8fead1f49b69",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}