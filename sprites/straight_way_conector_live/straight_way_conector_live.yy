{
    "id": "d834c99f-66c4-431b-9801-9fd8c6b26b08",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "straight_way_conector_live",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "27aadbdd-307f-4030-9cca-8c3295a53472",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d834c99f-66c4-431b-9801-9fd8c6b26b08",
            "compositeImage": {
                "id": "8d617862-4c4f-4956-aeab-5340b334c7a4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "27aadbdd-307f-4030-9cca-8c3295a53472",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "90280ba0-0fd8-4384-8f8e-eab44b8e5499",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "27aadbdd-307f-4030-9cca-8c3295a53472",
                    "LayerId": "df776469-f835-4687-bfa3-d3ad726d7fa8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "df776469-f835-4687-bfa3-d3ad726d7fa8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d834c99f-66c4-431b-9801-9fd8c6b26b08",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}