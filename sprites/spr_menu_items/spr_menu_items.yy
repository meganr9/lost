{
    "id": "ce468e40-75a5-4168-b941-80e91199917e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_menu_items",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 207,
    "bbox_left": 27,
    "bbox_right": 347,
    "bbox_top": 105,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a1f4a53e-e9e6-4c36-a68e-8d730eb975d2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ce468e40-75a5-4168-b941-80e91199917e",
            "compositeImage": {
                "id": "9ccf5537-b037-4341-94f5-a779a3ffa1fd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a1f4a53e-e9e6-4c36-a68e-8d730eb975d2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0e552910-6f7d-40c0-8d08-9fdd51ab9d73",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a1f4a53e-e9e6-4c36-a68e-8d730eb975d2",
                    "LayerId": "8b179c17-561f-4e55-a053-195d170d53c0"
                }
            ]
        },
        {
            "id": "25004201-43f8-463e-8970-fa482e966850",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ce468e40-75a5-4168-b941-80e91199917e",
            "compositeImage": {
                "id": "55909b82-3832-411d-9ad3-84b83797fb98",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "25004201-43f8-463e-8970-fa482e966850",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "55a8e6b7-0923-458e-8655-2f7ca25e1417",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "25004201-43f8-463e-8970-fa482e966850",
                    "LayerId": "8b179c17-561f-4e55-a053-195d170d53c0"
                }
            ]
        },
        {
            "id": "ca7e1731-1dab-4cf6-835c-55425059b4ad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ce468e40-75a5-4168-b941-80e91199917e",
            "compositeImage": {
                "id": "4748fa07-a7fd-4f74-808b-db84b8967cb3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ca7e1731-1dab-4cf6-835c-55425059b4ad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c5a939d2-a237-48d4-a977-af818ae4d68e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ca7e1731-1dab-4cf6-835c-55425059b4ad",
                    "LayerId": "8b179c17-561f-4e55-a053-195d170d53c0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 300,
    "layers": [
        {
            "id": "8b179c17-561f-4e55-a053-195d170d53c0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ce468e40-75a5-4168-b941-80e91199917e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 400,
    "xorig": 0,
    "yorig": 0
}