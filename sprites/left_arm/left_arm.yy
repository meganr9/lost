{
    "id": "e3444259-29a1-43b2-9358-3f69ea9a49ae",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "left_arm",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 246,
    "bbox_left": 131,
    "bbox_right": 309,
    "bbox_top": 194,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "5eee5128-6ee3-4fb6-a756-c0da7e830e18",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e3444259-29a1-43b2-9358-3f69ea9a49ae",
            "compositeImage": {
                "id": "a55de7bc-0fbb-4abc-8f74-e8ddc94df959",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5eee5128-6ee3-4fb6-a756-c0da7e830e18",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f08187ab-c94b-470b-b771-58ba2674951c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5eee5128-6ee3-4fb6-a756-c0da7e830e18",
                    "LayerId": "606aca0f-ef16-4b10-ad68-ce9860ac2c63"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 360,
    "layers": [
        {
            "id": "606aca0f-ef16-4b10-ad68-ce9860ac2c63",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e3444259-29a1-43b2-9358-3f69ea9a49ae",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 480,
    "xorig": 214,
    "yorig": 232
}