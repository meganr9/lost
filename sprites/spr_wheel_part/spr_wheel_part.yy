{
    "id": "4856ec22-efce-4789-bfee-485c6f951caa",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_wheel_part",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 359,
    "bbox_left": 179,
    "bbox_right": 273,
    "bbox_top": 298,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "e5a06f8f-d698-4c57-91ea-13e00ee462a4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4856ec22-efce-4789-bfee-485c6f951caa",
            "compositeImage": {
                "id": "829c053e-f5c5-4f50-abc2-91972d5053c2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e5a06f8f-d698-4c57-91ea-13e00ee462a4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "03302dbe-0702-486d-b3f9-ab1df3517038",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e5a06f8f-d698-4c57-91ea-13e00ee462a4",
                    "LayerId": "53c9f99c-56fc-4c34-bd0c-3cc7e3ab4147"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 360,
    "layers": [
        {
            "id": "53c9f99c-56fc-4c34-bd0c-3cc7e3ab4147",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4856ec22-efce-4789-bfee-485c6f951caa",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 480,
    "xorig": 222,
    "yorig": 322
}