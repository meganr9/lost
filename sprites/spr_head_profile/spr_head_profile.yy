{
    "id": "575e847a-4c51-41bf-a16d-64f01adef958",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_head_profile",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 117,
    "bbox_left": 266,
    "bbox_right": 338,
    "bbox_top": 40,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "31ca6d55-2382-4dbc-a531-7b794855003d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "575e847a-4c51-41bf-a16d-64f01adef958",
            "compositeImage": {
                "id": "b41d18c2-2264-45b8-b94a-d3893a3c6176",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "31ca6d55-2382-4dbc-a531-7b794855003d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8495f859-049e-4975-a87e-58e141ca1b25",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "31ca6d55-2382-4dbc-a531-7b794855003d",
                    "LayerId": "473106c8-47ab-47f9-8df3-40cd42844ac5"
                }
            ]
        },
        {
            "id": "e1d207d6-d25d-4fdd-8dfd-e0bfac58aa53",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "575e847a-4c51-41bf-a16d-64f01adef958",
            "compositeImage": {
                "id": "fc788a4f-5862-43ca-91a1-a17f6406a610",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e1d207d6-d25d-4fdd-8dfd-e0bfac58aa53",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c69eebee-3433-4977-a358-5850e430a33e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e1d207d6-d25d-4fdd-8dfd-e0bfac58aa53",
                    "LayerId": "473106c8-47ab-47f9-8df3-40cd42844ac5"
                }
            ]
        },
        {
            "id": "5974baae-5799-4eda-84bb-e7ccb6e80ec1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "575e847a-4c51-41bf-a16d-64f01adef958",
            "compositeImage": {
                "id": "9bc2f600-0033-415f-acca-77d5e5d47df0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5974baae-5799-4eda-84bb-e7ccb6e80ec1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f1d47864-4b0c-4374-8afc-6a149504e906",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5974baae-5799-4eda-84bb-e7ccb6e80ec1",
                    "LayerId": "473106c8-47ab-47f9-8df3-40cd42844ac5"
                }
            ]
        },
        {
            "id": "77e0698a-4492-44e0-aeec-ad4804b368e3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "575e847a-4c51-41bf-a16d-64f01adef958",
            "compositeImage": {
                "id": "ca4298d5-f5d9-49cd-aa47-9ff4f0201f7b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "77e0698a-4492-44e0-aeec-ad4804b368e3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1ad01648-fa43-4912-85d7-fc7e2c39e1f3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "77e0698a-4492-44e0-aeec-ad4804b368e3",
                    "LayerId": "473106c8-47ab-47f9-8df3-40cd42844ac5"
                }
            ]
        },
        {
            "id": "9a64d795-649e-4df2-a5a7-8ca090a6d5e1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "575e847a-4c51-41bf-a16d-64f01adef958",
            "compositeImage": {
                "id": "1cea381d-d8d9-41d1-9727-6ac41d95697c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9a64d795-649e-4df2-a5a7-8ca090a6d5e1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9008a941-3bdc-439a-bf1d-0663179e8200",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9a64d795-649e-4df2-a5a7-8ca090a6d5e1",
                    "LayerId": "473106c8-47ab-47f9-8df3-40cd42844ac5"
                }
            ]
        },
        {
            "id": "af36d377-d90b-40aa-9fa0-c8dfa55f4f1a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "575e847a-4c51-41bf-a16d-64f01adef958",
            "compositeImage": {
                "id": "a713f200-2949-4f11-beb1-1e87fe34d281",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "af36d377-d90b-40aa-9fa0-c8dfa55f4f1a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6f9f8c8c-8697-4b5d-8d3c-90b9a22540a6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "af36d377-d90b-40aa-9fa0-c8dfa55f4f1a",
                    "LayerId": "473106c8-47ab-47f9-8df3-40cd42844ac5"
                }
            ]
        },
        {
            "id": "deced6f7-3e8a-48f0-ac59-e1d8490bdd2f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "575e847a-4c51-41bf-a16d-64f01adef958",
            "compositeImage": {
                "id": "ba509d94-4ac3-44b6-bf34-17d70549a399",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "deced6f7-3e8a-48f0-ac59-e1d8490bdd2f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2269231d-8e9c-4252-be48-99eb11793c18",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "deced6f7-3e8a-48f0-ac59-e1d8490bdd2f",
                    "LayerId": "473106c8-47ab-47f9-8df3-40cd42844ac5"
                }
            ]
        },
        {
            "id": "33ca7f3e-89b4-49c7-b7fb-7fe1e75d27fc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "575e847a-4c51-41bf-a16d-64f01adef958",
            "compositeImage": {
                "id": "f0114d4c-30da-4bac-8c10-82d3a4a78f95",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "33ca7f3e-89b4-49c7-b7fb-7fe1e75d27fc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "56038c20-dff3-40a2-9ce5-ec7e36eca985",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "33ca7f3e-89b4-49c7-b7fb-7fe1e75d27fc",
                    "LayerId": "473106c8-47ab-47f9-8df3-40cd42844ac5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 480,
    "layers": [
        {
            "id": "473106c8-47ab-47f9-8df3-40cd42844ac5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "575e847a-4c51-41bf-a16d-64f01adef958",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 640,
    "xorig": 0,
    "yorig": 0
}