{
    "id": "757058ca-e1b9-4557-81c6-94ab50f733e1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spri_add_2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 478,
    "bbox_left": 8,
    "bbox_right": 542,
    "bbox_top": 45,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "71d61bc6-d485-4fac-9caa-6ba3dad3b96d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "757058ca-e1b9-4557-81c6-94ab50f733e1",
            "compositeImage": {
                "id": "0e41fc34-a8ce-4159-9b8e-28777dd60d5d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "71d61bc6-d485-4fac-9caa-6ba3dad3b96d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "36f8839d-c617-4da3-be32-67b7c2261743",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "71d61bc6-d485-4fac-9caa-6ba3dad3b96d",
                    "LayerId": "47eb609d-7c0b-4308-8dc6-7a4c91f451a5"
                }
            ]
        },
        {
            "id": "d4d549c0-29d6-4bc7-b841-c6ff9d340b44",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "757058ca-e1b9-4557-81c6-94ab50f733e1",
            "compositeImage": {
                "id": "c2e880ae-c77c-474c-a1cc-b38caf2a9512",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d4d549c0-29d6-4bc7-b841-c6ff9d340b44",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1142ee6e-7b75-49f9-85c4-f8c0cb9bc290",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d4d549c0-29d6-4bc7-b841-c6ff9d340b44",
                    "LayerId": "47eb609d-7c0b-4308-8dc6-7a4c91f451a5"
                }
            ]
        },
        {
            "id": "4f9a985c-5b5e-4a4d-8ee8-37141e83485b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "757058ca-e1b9-4557-81c6-94ab50f733e1",
            "compositeImage": {
                "id": "96bc11ac-708d-436e-ae2d-15b845f511f1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4f9a985c-5b5e-4a4d-8ee8-37141e83485b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "81c32205-76f2-4acf-a206-062086e7761b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4f9a985c-5b5e-4a4d-8ee8-37141e83485b",
                    "LayerId": "47eb609d-7c0b-4308-8dc6-7a4c91f451a5"
                }
            ]
        },
        {
            "id": "4d97c350-c147-4585-bbcc-b246b6246589",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "757058ca-e1b9-4557-81c6-94ab50f733e1",
            "compositeImage": {
                "id": "43789ee0-3b7a-405c-bf0d-fa3813b67afa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4d97c350-c147-4585-bbcc-b246b6246589",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "23afc2ce-d335-4997-b804-28c9225e3320",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4d97c350-c147-4585-bbcc-b246b6246589",
                    "LayerId": "47eb609d-7c0b-4308-8dc6-7a4c91f451a5"
                }
            ]
        },
        {
            "id": "1d66fc3b-9c70-4fc8-be60-b3c07e11e3aa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "757058ca-e1b9-4557-81c6-94ab50f733e1",
            "compositeImage": {
                "id": "dfb1525b-4638-4be6-9e9a-025be42af9e6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1d66fc3b-9c70-4fc8-be60-b3c07e11e3aa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d1366d20-1bbe-41d6-8f92-9c98b21b1973",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1d66fc3b-9c70-4fc8-be60-b3c07e11e3aa",
                    "LayerId": "47eb609d-7c0b-4308-8dc6-7a4c91f451a5"
                }
            ]
        },
        {
            "id": "289597eb-4ee5-4709-b0a0-4c856717072f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "757058ca-e1b9-4557-81c6-94ab50f733e1",
            "compositeImage": {
                "id": "f5fbd83e-ad4a-48c8-90d8-712bd0e9a50b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "289597eb-4ee5-4709-b0a0-4c856717072f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "551f8140-3b33-4804-989e-2e816cde6797",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "289597eb-4ee5-4709-b0a0-4c856717072f",
                    "LayerId": "47eb609d-7c0b-4308-8dc6-7a4c91f451a5"
                }
            ]
        },
        {
            "id": "4f21956e-6d53-4fad-8a0f-ba88987b9430",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "757058ca-e1b9-4557-81c6-94ab50f733e1",
            "compositeImage": {
                "id": "ae2fc897-caff-4cbb-b00e-2b506be558c4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4f21956e-6d53-4fad-8a0f-ba88987b9430",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e31ba362-1526-42c0-957d-856028522aa3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4f21956e-6d53-4fad-8a0f-ba88987b9430",
                    "LayerId": "47eb609d-7c0b-4308-8dc6-7a4c91f451a5"
                }
            ]
        },
        {
            "id": "9a3bc9f9-2a9e-4007-a8a1-36ffba69bab4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "757058ca-e1b9-4557-81c6-94ab50f733e1",
            "compositeImage": {
                "id": "997671d0-aa0d-4cb6-8f3b-57cc4f373036",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9a3bc9f9-2a9e-4007-a8a1-36ffba69bab4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cb02eab0-c820-433d-9438-d123cd421c5d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9a3bc9f9-2a9e-4007-a8a1-36ffba69bab4",
                    "LayerId": "47eb609d-7c0b-4308-8dc6-7a4c91f451a5"
                }
            ]
        },
        {
            "id": "847611c2-4466-436e-90e2-aeb63d2ad672",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "757058ca-e1b9-4557-81c6-94ab50f733e1",
            "compositeImage": {
                "id": "77829138-bbc0-4b8d-9ede-f724f4b0a214",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "847611c2-4466-436e-90e2-aeb63d2ad672",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "86866923-56eb-4568-b959-c19e0bb010cc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "847611c2-4466-436e-90e2-aeb63d2ad672",
                    "LayerId": "47eb609d-7c0b-4308-8dc6-7a4c91f451a5"
                }
            ]
        },
        {
            "id": "9b75a20f-28e3-462d-927a-2436b4229d8d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "757058ca-e1b9-4557-81c6-94ab50f733e1",
            "compositeImage": {
                "id": "bd98e944-f961-47fc-84c7-0ea138d58e82",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9b75a20f-28e3-462d-927a-2436b4229d8d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "905ce9c6-7983-4874-8997-940ee845b49f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9b75a20f-28e3-462d-927a-2436b4229d8d",
                    "LayerId": "47eb609d-7c0b-4308-8dc6-7a4c91f451a5"
                }
            ]
        },
        {
            "id": "3cd5ffcb-8b35-4ca5-81c2-35832362edb0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "757058ca-e1b9-4557-81c6-94ab50f733e1",
            "compositeImage": {
                "id": "c453ca90-0569-411f-aa26-3220105f4fb2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3cd5ffcb-8b35-4ca5-81c2-35832362edb0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "26fadb04-5cc5-44c1-b002-183310a31981",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3cd5ffcb-8b35-4ca5-81c2-35832362edb0",
                    "LayerId": "47eb609d-7c0b-4308-8dc6-7a4c91f451a5"
                }
            ]
        },
        {
            "id": "ac5dbac0-bef8-4221-97dd-532115e4c082",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "757058ca-e1b9-4557-81c6-94ab50f733e1",
            "compositeImage": {
                "id": "9d2ff8fa-2ba2-4791-90b6-ef7f058149ec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ac5dbac0-bef8-4221-97dd-532115e4c082",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8a21051f-620e-4e9f-9538-69fd3d354a95",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ac5dbac0-bef8-4221-97dd-532115e4c082",
                    "LayerId": "47eb609d-7c0b-4308-8dc6-7a4c91f451a5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 480,
    "layers": [
        {
            "id": "47eb609d-7c0b-4308-8dc6-7a4c91f451a5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "757058ca-e1b9-4557-81c6-94ab50f733e1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 640,
    "xorig": 0,
    "yorig": 0
}