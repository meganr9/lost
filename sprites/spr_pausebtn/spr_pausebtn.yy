{
    "id": "d0ec4930-db7c-49dd-a705-2ae047f4a997",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_pausebtn",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 61,
    "bbox_left": 4,
    "bbox_right": 99,
    "bbox_top": 25,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "637fe459-82d3-4881-a379-59ef3d5715f4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d0ec4930-db7c-49dd-a705-2ae047f4a997",
            "compositeImage": {
                "id": "cb9ff5f6-3dd2-4ac9-996f-da8c5db7ebdb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "637fe459-82d3-4881-a379-59ef3d5715f4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "afd3b0da-2c3b-446d-9dcb-236386cb7899",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "637fe459-82d3-4881-a379-59ef3d5715f4",
                    "LayerId": "75c58a68-eeea-47ff-8768-314b97f8df03"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 100,
    "layers": [
        {
            "id": "75c58a68-eeea-47ff-8768-314b97f8df03",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d0ec4930-db7c-49dd-a705-2ae047f4a997",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 100,
    "xorig": 25,
    "yorig": 30
}