{
    "id": "8174420a-4711-4071-8d4a-412255867f77",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "straight_way_conector",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0e4858a8-3a65-47ad-a3b0-19835aa2e7dc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8174420a-4711-4071-8d4a-412255867f77",
            "compositeImage": {
                "id": "f66d4f58-d747-4a39-bf94-3457efe103ab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0e4858a8-3a65-47ad-a3b0-19835aa2e7dc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bbcc206c-ae05-4c2b-9338-1748bf4ff75e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0e4858a8-3a65-47ad-a3b0-19835aa2e7dc",
                    "LayerId": "b0e2f271-3721-4be9-93dc-d6c0c593e6bc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "b0e2f271-3721-4be9-93dc-d6c0c593e6bc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8174420a-4711-4071-8d4a-412255867f77",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}