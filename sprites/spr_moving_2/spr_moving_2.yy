{
    "id": "6b67e821-f0af-4a6a-89d9-5d6a390ed40a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_moving_2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 477,
    "bbox_left": 224,
    "bbox_right": 399,
    "bbox_top": 41,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "2364810f-2868-415b-abd8-64cc05e8dd12",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6b67e821-f0af-4a6a-89d9-5d6a390ed40a",
            "compositeImage": {
                "id": "007b13de-8211-4ae7-a900-501b4a2a370d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2364810f-2868-415b-abd8-64cc05e8dd12",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "454d83ef-291d-4a72-b553-f2bb411a9c31",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2364810f-2868-415b-abd8-64cc05e8dd12",
                    "LayerId": "6c19023a-b61f-4b79-a598-ca56b2f06751"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 480,
    "layers": [
        {
            "id": "6c19023a-b61f-4b79-a598-ca56b2f06751",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6b67e821-f0af-4a6a-89d9-5d6a390ed40a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 640,
    "xorig": 0,
    "yorig": 0
}