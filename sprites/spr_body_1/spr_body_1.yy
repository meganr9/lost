{
    "id": "ea1e2e34-f4a4-48aa-8bab-ab1abdaea274",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_body_1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 475,
    "bbox_left": 174,
    "bbox_right": 314,
    "bbox_top": 297,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "9b024275-fd8b-4340-9891-208b2cab9f04",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ea1e2e34-f4a4-48aa-8bab-ab1abdaea274",
            "compositeImage": {
                "id": "c1c299ef-0844-40b8-bb27-2fc554a5a4ad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9b024275-fd8b-4340-9891-208b2cab9f04",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7cfe3a4e-0dd7-47c7-9966-f4b0583a9e30",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9b024275-fd8b-4340-9891-208b2cab9f04",
                    "LayerId": "928367fb-d09a-467e-818a-5787150f402a"
                }
            ]
        },
        {
            "id": "35f7b619-86b6-4519-ab21-e9d95b3dfc0b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ea1e2e34-f4a4-48aa-8bab-ab1abdaea274",
            "compositeImage": {
                "id": "f4877b86-9793-4eb5-b7b5-b8f592f5ddac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "35f7b619-86b6-4519-ab21-e9d95b3dfc0b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d84d78ae-5819-4400-a19f-2894c3b6699b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "35f7b619-86b6-4519-ab21-e9d95b3dfc0b",
                    "LayerId": "928367fb-d09a-467e-818a-5787150f402a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 480,
    "layers": [
        {
            "id": "928367fb-d09a-467e-818a-5787150f402a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ea1e2e34-f4a4-48aa-8bab-ab1abdaea274",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 640,
    "xorig": 0,
    "yorig": 0
}