{
    "id": "6d3b30ec-3f14-4492-a413-1c181163f8be",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_dead_end",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "6403a297-4451-4da3-9555-01e6d45f5ff7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6d3b30ec-3f14-4492-a413-1c181163f8be",
            "compositeImage": {
                "id": "0bed48f5-cc8b-43ae-93ed-7dee8bbf04eb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6403a297-4451-4da3-9555-01e6d45f5ff7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2268912f-9767-47d0-b6bd-0ae7d3f82baf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6403a297-4451-4da3-9555-01e6d45f5ff7",
                    "LayerId": "0c2d3e66-e3b2-4934-8943-109f472a1e7c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "0c2d3e66-e3b2-4934-8943-109f472a1e7c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6d3b30ec-3f14-4492-a413-1c181163f8be",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 31,
    "yorig": 29
}