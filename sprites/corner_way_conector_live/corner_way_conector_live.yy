{
    "id": "9019ba80-5d78-48d4-a1ca-1cb26575295a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "corner_way_conector_live",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d673519d-3fc0-4afd-8d2f-3fd57730ebce",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9019ba80-5d78-48d4-a1ca-1cb26575295a",
            "compositeImage": {
                "id": "10ceb0ae-cdb9-4dc2-8e83-85f0ca7276cd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d673519d-3fc0-4afd-8d2f-3fd57730ebce",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "46a01583-59db-42d8-a511-d8b159656627",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d673519d-3fc0-4afd-8d2f-3fd57730ebce",
                    "LayerId": "7ebdb760-db46-4ebf-98fc-0f9ae3f52e23"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "7ebdb760-db46-4ebf-98fc-0f9ae3f52e23",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9019ba80-5d78-48d4-a1ca-1cb26575295a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}