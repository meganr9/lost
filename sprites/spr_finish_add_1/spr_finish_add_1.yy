{
    "id": "fd2585e1-1dcf-4b41-ac33-af7776820422",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_finish_add_1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 238,
    "bbox_left": 125,
    "bbox_right": 197,
    "bbox_top": 161,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "dac7e51b-a3ca-4974-99eb-b1ea120b7094",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd2585e1-1dcf-4b41-ac33-af7776820422",
            "compositeImage": {
                "id": "58fc07b9-2bd4-4818-84eb-9289d4696df4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dac7e51b-a3ca-4974-99eb-b1ea120b7094",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ad02bb5b-a2bd-4bd8-a1d8-0245176cc16e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dac7e51b-a3ca-4974-99eb-b1ea120b7094",
                    "LayerId": "7a8f3e91-aeff-472c-86a9-609dec5b0ee1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 240,
    "layers": [
        {
            "id": "7a8f3e91-aeff-472c-86a9-609dec5b0ee1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fd2585e1-1dcf-4b41-ac33-af7776820422",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 320,
    "xorig": 0,
    "yorig": 238
}