{
    "id": "e1cd2247-74eb-48c3-8754-588315766421",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_body_part1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 264,
    "bbox_left": 129,
    "bbox_right": 297,
    "bbox_top": 117,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "frames": [
        {
            "id": "1c469115-d07d-48ef-b9e5-1db8bec5b853",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1cd2247-74eb-48c3-8754-588315766421",
            "compositeImage": {
                "id": "5a31597a-195d-418e-a486-c53bfa9d48b8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1c469115-d07d-48ef-b9e5-1db8bec5b853",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "596687cf-ecc9-429b-8b43-8ed151812ce9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1c469115-d07d-48ef-b9e5-1db8bec5b853",
                    "LayerId": "72c0e475-208a-4da5-bdde-a699f687cd11"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 360,
    "layers": [
        {
            "id": "72c0e475-208a-4da5-bdde-a699f687cd11",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e1cd2247-74eb-48c3-8754-588315766421",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": true,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 480,
    "xorig": 223,
    "yorig": 158
}