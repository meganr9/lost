{
    "id": "8f7ec581-1aa1-4bfa-8d17-901790da1d8a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_moving_left",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 473,
    "bbox_left": 156,
    "bbox_right": 482,
    "bbox_top": 33,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "8ffa9e37-82f9-4faa-becd-89b4baed7bec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8f7ec581-1aa1-4bfa-8d17-901790da1d8a",
            "compositeImage": {
                "id": "0ba25f35-11ab-44a7-af49-d401113c07fb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8ffa9e37-82f9-4faa-becd-89b4baed7bec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "61f04fde-093f-4e47-a41b-12637cec7511",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8ffa9e37-82f9-4faa-becd-89b4baed7bec",
                    "LayerId": "dfaa6522-b2f4-4311-9be3-a99254bdbd95"
                }
            ]
        },
        {
            "id": "05e9c694-48f0-4f60-9d78-a7ae7582e9eb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8f7ec581-1aa1-4bfa-8d17-901790da1d8a",
            "compositeImage": {
                "id": "5312b4e8-0520-4c8b-9145-195adb3799f0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "05e9c694-48f0-4f60-9d78-a7ae7582e9eb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e5f137ab-f610-4924-8e0c-f80c7e2a0224",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "05e9c694-48f0-4f60-9d78-a7ae7582e9eb",
                    "LayerId": "dfaa6522-b2f4-4311-9be3-a99254bdbd95"
                }
            ]
        },
        {
            "id": "1e7ae023-5f54-445c-87c8-40a9eae87215",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8f7ec581-1aa1-4bfa-8d17-901790da1d8a",
            "compositeImage": {
                "id": "0052be8a-8462-4a67-92ba-f140cdf0032c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1e7ae023-5f54-445c-87c8-40a9eae87215",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2754d59d-f35d-4125-a2f2-41d4cf551bf0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1e7ae023-5f54-445c-87c8-40a9eae87215",
                    "LayerId": "dfaa6522-b2f4-4311-9be3-a99254bdbd95"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 480,
    "layers": [
        {
            "id": "dfaa6522-b2f4-4311-9be3-a99254bdbd95",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8f7ec581-1aa1-4bfa-8d17-901790da1d8a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 640,
    "xorig": 0,
    "yorig": 0
}