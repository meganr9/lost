{
    "id": "2dcdf7df-437d-474c-94c3-02bccaf05d31",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "set_trap",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "549699f0-20f3-4b1c-9134-3208e5e81114",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2dcdf7df-437d-474c-94c3-02bccaf05d31",
            "compositeImage": {
                "id": "c276066b-a656-468e-8372-75cf44817f37",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "549699f0-20f3-4b1c-9134-3208e5e81114",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f0772964-be91-4b53-9e0c-4c5351ae5860",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "549699f0-20f3-4b1c-9134-3208e5e81114",
                    "LayerId": "55a88a6c-c76a-4512-a34c-30d31f16aff3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "55a88a6c-c76a-4512-a34c-30d31f16aff3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2dcdf7df-437d-474c-94c3-02bccaf05d31",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}