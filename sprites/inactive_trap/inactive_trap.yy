{
    "id": "89fd0eb1-0331-45ff-9b00-4e1437b67d4c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "inactive_trap",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "814bbcff-dc7d-477e-b685-3f03de8d749d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "89fd0eb1-0331-45ff-9b00-4e1437b67d4c",
            "compositeImage": {
                "id": "f4d19909-026b-46a6-b4e6-2d05d678c839",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "814bbcff-dc7d-477e-b685-3f03de8d749d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a3e1e675-1440-4e39-858c-f8faad484f2c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "814bbcff-dc7d-477e-b685-3f03de8d749d",
                    "LayerId": "e23603a7-d08e-4532-bf81-b1fbeffebac3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "e23603a7-d08e-4532-bf81-b1fbeffebac3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "89fd0eb1-0331-45ff-9b00-4e1437b67d4c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}