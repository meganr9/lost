{
    "id": "676e4625-9a31-431b-a6c3-1355c03cbc34",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "four_way_conector_live",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6bf4f58e-7ba4-421b-8a92-3bc1844eb73e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "676e4625-9a31-431b-a6c3-1355c03cbc34",
            "compositeImage": {
                "id": "b050a99f-9725-4a58-86f8-6a76b5d48806",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6bf4f58e-7ba4-421b-8a92-3bc1844eb73e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9baf5f86-7d31-42e1-aca4-3f8a85932375",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6bf4f58e-7ba4-421b-8a92-3bc1844eb73e",
                    "LayerId": "11a21f31-e9cd-47fc-999f-a25b297cf147"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "11a21f31-e9cd-47fc-999f-a25b297cf147",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "676e4625-9a31-431b-a6c3-1355c03cbc34",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}