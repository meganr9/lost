{
    "id": "8b183681-72c9-4f9a-93c5-202f6505ca96",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "maze_robot_player",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 86,
    "bbox_left": 201,
    "bbox_right": 252,
    "bbox_top": 34,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c50906fc-915b-4945-a5f2-46c551630d14",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8b183681-72c9-4f9a-93c5-202f6505ca96",
            "compositeImage": {
                "id": "1070ba7b-1f9d-498b-a5e2-57080932cea0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c50906fc-915b-4945-a5f2-46c551630d14",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "05038fa0-beb6-4a7c-a0ce-0f52fabb2328",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c50906fc-915b-4945-a5f2-46c551630d14",
                    "LayerId": "8ae4c7ac-c99b-4643-8539-3b547a0d9847"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 360,
    "layers": [
        {
            "id": "8ae4c7ac-c99b-4643-8539-3b547a0d9847",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8b183681-72c9-4f9a-93c5-202f6505ca96",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 480,
    "xorig": 225,
    "yorig": 58
}