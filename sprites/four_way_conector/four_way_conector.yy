{
    "id": "848e46c0-fe5a-482e-ab0d-413260518a6e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "four_way_conector",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f1ac660b-906d-487d-b7e8-6a21e9e9f310",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "848e46c0-fe5a-482e-ab0d-413260518a6e",
            "compositeImage": {
                "id": "e32afba4-23c0-46fa-81c2-02007d2775fd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f1ac660b-906d-487d-b7e8-6a21e9e9f310",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d9863af8-98e7-4320-81db-29f2b6ad9040",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f1ac660b-906d-487d-b7e8-6a21e9e9f310",
                    "LayerId": "7b4d3d77-942b-4175-b618-77193fc8f88f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "7b4d3d77-942b-4175-b618-77193fc8f88f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "848e46c0-fe5a-482e-ab0d-413260518a6e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}