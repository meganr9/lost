{
    "id": "09391275-41d5-4679-b671-e9c9c22c5045",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_next_rmA",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "a90b41b7-ba68-4c7b-ba6c-d31199933519",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "09391275-41d5-4679-b671-e9c9c22c5045",
            "compositeImage": {
                "id": "a65b45b4-7f17-4c39-aab9-84dbd93d7f42",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a90b41b7-ba68-4c7b-ba6c-d31199933519",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2f72c7bc-a5d1-4b9a-858c-727263015ba1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a90b41b7-ba68-4c7b-ba6c-d31199933519",
                    "LayerId": "3af1e481-a82c-4028-9b06-8b8ea6515a01"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "3af1e481-a82c-4028-9b06-8b8ea6515a01",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "09391275-41d5-4679-b671-e9c9c22c5045",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}