{
    "id": "d03e8802-78b0-490c-bd20-40ba03ccebee",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "active_trap",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2b6891b9-d13d-4fd3-aeb4-8cf4c75ca8ca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d03e8802-78b0-490c-bd20-40ba03ccebee",
            "compositeImage": {
                "id": "936f1904-e00c-4916-b613-7dfe18212d83",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2b6891b9-d13d-4fd3-aeb4-8cf4c75ca8ca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "56698550-4866-4b4f-9e64-bc9d69ad2679",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2b6891b9-d13d-4fd3-aeb4-8cf4c75ca8ca",
                    "LayerId": "a9c7236c-0f1e-4c8e-9bff-77bcbbc58b52"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "a9c7236c-0f1e-4c8e-9bff-77bcbbc58b52",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d03e8802-78b0-490c-bd20-40ba03ccebee",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}