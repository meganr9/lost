{
    "id": "a6ef1554-56f0-48c2-8862-90304e6be24b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "bump",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c9e73e94-52e9-425b-bcb4-b84c719c4260",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6ef1554-56f0-48c2-8862-90304e6be24b",
            "compositeImage": {
                "id": "9fa688f5-0cdf-4dd0-8bd0-1cc978239268",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c9e73e94-52e9-425b-bcb4-b84c719c4260",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "74b10b77-a750-4f0d-8d9f-beb272d62071",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c9e73e94-52e9-425b-bcb4-b84c719c4260",
                    "LayerId": "c54f1fe1-b25a-4f79-a3a5-ce9ff3d58d4b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "c54f1fe1-b25a-4f79-a3a5-ce9ff3d58d4b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a6ef1554-56f0-48c2-8862-90304e6be24b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}