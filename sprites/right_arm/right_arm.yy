{
    "id": "60ea6949-62b1-4e30-9007-90a8167eb556",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "right_arm",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 246,
    "bbox_left": 170,
    "bbox_right": 348,
    "bbox_top": 194,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "7c86f22f-44e3-4a3c-99ca-65b5b1e65352",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "60ea6949-62b1-4e30-9007-90a8167eb556",
            "compositeImage": {
                "id": "cb4421d4-46f2-4c6e-9db3-f77e9dc8fd60",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7c86f22f-44e3-4a3c-99ca-65b5b1e65352",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "70a9da8f-b8ff-43cb-968d-b3a2aad9316a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7c86f22f-44e3-4a3c-99ca-65b5b1e65352",
                    "LayerId": "683d0b3c-0af7-4b55-98d2-25d2f8bd5f90"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 360,
    "layers": [
        {
            "id": "683d0b3c-0af7-4b55-98d2-25d2f8bd5f90",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "60ea6949-62b1-4e30-9007-90a8167eb556",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 480,
    "xorig": 250,
    "yorig": 222
}