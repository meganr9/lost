{
    "id": "197f5dea-fe2d-4566-b1bc-019e19acc251",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_next_room",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "89a9cfb3-6118-4b96-890b-f67d58d6c39b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "197f5dea-fe2d-4566-b1bc-019e19acc251",
            "compositeImage": {
                "id": "4b46a2f6-211b-44d4-acb4-4d7f7937940e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "89a9cfb3-6118-4b96-890b-f67d58d6c39b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fa379178-a6b0-4c1a-819f-76477738384c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "89a9cfb3-6118-4b96-890b-f67d58d6c39b",
                    "LayerId": "08c7c406-a29c-41a5-abd7-e9f0242c64f9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "08c7c406-a29c-41a5-abd7-e9f0242c64f9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "197f5dea-fe2d-4566-b1bc-019e19acc251",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 30,
    "yorig": 32
}