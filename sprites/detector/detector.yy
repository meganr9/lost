{
    "id": "d6431460-80ba-4c11-913a-b1a4e05c93bb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "detector",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 61,
    "bbox_left": 35,
    "bbox_right": 94,
    "bbox_top": 14,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "5ea49788-85b2-47d1-837e-c76701ce7a8b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d6431460-80ba-4c11-913a-b1a4e05c93bb",
            "compositeImage": {
                "id": "b4448066-2eb4-430c-be90-fc9cc9994d0f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5ea49788-85b2-47d1-837e-c76701ce7a8b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0043da1a-4f58-49df-b275-169ff09328b3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5ea49788-85b2-47d1-837e-c76701ce7a8b",
                    "LayerId": "2b60e4d8-c9ba-4d15-9b4e-7883454ffc24"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 90,
    "layers": [
        {
            "id": "2b60e4d8-c9ba-4d15-9b4e-7883454ffc24",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d6431460-80ba-4c11-913a-b1a4e05c93bb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 120,
    "xorig": 0,
    "yorig": 0
}