{
    "id": "44d2bd1d-0fef-4be4-9005-d6c4da67940e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_body_part2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 292,
    "bbox_left": 198,
    "bbox_right": 308,
    "bbox_top": 166,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "frames": [
        {
            "id": "887e3a42-3059-4e07-bc90-4664c4903e79",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "44d2bd1d-0fef-4be4-9005-d6c4da67940e",
            "compositeImage": {
                "id": "cf87d0d9-e1a4-4b92-9723-49bcb8ad93a9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "887e3a42-3059-4e07-bc90-4664c4903e79",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b51fadc2-cc68-4814-8270-6758a7df1695",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "887e3a42-3059-4e07-bc90-4664c4903e79",
                    "LayerId": "61cd32cf-4fb7-420f-956e-6233ccb6a23d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 360,
    "layers": [
        {
            "id": "61cd32cf-4fb7-420f-956e-6233ccb6a23d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "44d2bd1d-0fef-4be4-9005-d6c4da67940e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 480,
    "xorig": 242,
    "yorig": 228
}