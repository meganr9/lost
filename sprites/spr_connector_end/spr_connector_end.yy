{
    "id": "72182f29-cee2-4956-99b3-c39a3bf6c74c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_connector_end",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6ffbe2bd-a9f3-46a6-ad3a-e24231839ad1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "72182f29-cee2-4956-99b3-c39a3bf6c74c",
            "compositeImage": {
                "id": "9ab3f8f1-cf7b-4448-a691-80918d52c439",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6ffbe2bd-a9f3-46a6-ad3a-e24231839ad1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0868a13d-e17f-415a-b0b2-e547cb20517e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6ffbe2bd-a9f3-46a6-ad3a-e24231839ad1",
                    "LayerId": "bc1f1a1e-2c34-47d2-b585-8e476345235f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "bc1f1a1e-2c34-47d2-b585-8e476345235f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "72182f29-cee2-4956-99b3-c39a3bf6c74c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}