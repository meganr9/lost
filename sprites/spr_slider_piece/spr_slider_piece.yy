{
    "id": "62bc7dde-32cc-4b1d-b457-c2de0d9aad9c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_slider_piece",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2340b4d1-9f8a-4899-af46-3811dbcd66ad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "62bc7dde-32cc-4b1d-b457-c2de0d9aad9c",
            "compositeImage": {
                "id": "739c8a61-7b86-4550-acf6-1f2722d684e9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2340b4d1-9f8a-4899-af46-3811dbcd66ad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4c7e1db9-acf6-4a62-9fe3-37f9303d52e7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2340b4d1-9f8a-4899-af46-3811dbcd66ad",
                    "LayerId": "ff399893-d5e0-4f6a-a1aa-5cef6e7c679f"
                }
            ]
        },
        {
            "id": "90709e30-4a9e-4351-b34d-1af051270830",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "62bc7dde-32cc-4b1d-b457-c2de0d9aad9c",
            "compositeImage": {
                "id": "9bd9d7fa-7393-4638-acce-6118f9512977",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "90709e30-4a9e-4351-b34d-1af051270830",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fb80b791-c7a8-44a9-8a15-b47b88923ff3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "90709e30-4a9e-4351-b34d-1af051270830",
                    "LayerId": "ff399893-d5e0-4f6a-a1aa-5cef6e7c679f"
                }
            ]
        },
        {
            "id": "c63ec51e-2ef2-44eb-a615-f0964a569a68",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "62bc7dde-32cc-4b1d-b457-c2de0d9aad9c",
            "compositeImage": {
                "id": "4eb00516-5ce3-495b-bf58-999cba57e4b9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c63ec51e-2ef2-44eb-a615-f0964a569a68",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "319c8a16-bc9b-45dc-a5a4-4feedf702a42",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c63ec51e-2ef2-44eb-a615-f0964a569a68",
                    "LayerId": "ff399893-d5e0-4f6a-a1aa-5cef6e7c679f"
                }
            ]
        },
        {
            "id": "b74a2bc2-b2e0-4eeb-a4de-6d7c3220afae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "62bc7dde-32cc-4b1d-b457-c2de0d9aad9c",
            "compositeImage": {
                "id": "264723bb-3287-4a06-9398-d23559153535",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b74a2bc2-b2e0-4eeb-a4de-6d7c3220afae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "51830900-6a0a-4265-8653-cf5eb4af749a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b74a2bc2-b2e0-4eeb-a4de-6d7c3220afae",
                    "LayerId": "ff399893-d5e0-4f6a-a1aa-5cef6e7c679f"
                }
            ]
        },
        {
            "id": "1a987b9d-9b0c-47ab-9155-eb1984a71a4b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "62bc7dde-32cc-4b1d-b457-c2de0d9aad9c",
            "compositeImage": {
                "id": "dc7060f6-3a7a-494b-892f-e32eac7305fa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1a987b9d-9b0c-47ab-9155-eb1984a71a4b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "64e40048-6f98-469c-9cc5-cff27920ebc1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1a987b9d-9b0c-47ab-9155-eb1984a71a4b",
                    "LayerId": "ff399893-d5e0-4f6a-a1aa-5cef6e7c679f"
                }
            ]
        },
        {
            "id": "7a4ac477-ca3a-4afe-8ceb-135457ae949a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "62bc7dde-32cc-4b1d-b457-c2de0d9aad9c",
            "compositeImage": {
                "id": "983f4992-9c20-4ac0-bc92-72c6b21fbcdc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7a4ac477-ca3a-4afe-8ceb-135457ae949a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f675e94a-50a3-4701-8450-5b28661b09f3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7a4ac477-ca3a-4afe-8ceb-135457ae949a",
                    "LayerId": "ff399893-d5e0-4f6a-a1aa-5cef6e7c679f"
                }
            ]
        },
        {
            "id": "62d25d94-c53b-4587-9313-f68e61b8a0d1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "62bc7dde-32cc-4b1d-b457-c2de0d9aad9c",
            "compositeImage": {
                "id": "e28721db-f689-4bb8-ba4c-c0892d8b8038",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "62d25d94-c53b-4587-9313-f68e61b8a0d1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4a2f59bc-30e4-4f00-96cb-3ddee2b37909",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "62d25d94-c53b-4587-9313-f68e61b8a0d1",
                    "LayerId": "ff399893-d5e0-4f6a-a1aa-5cef6e7c679f"
                }
            ]
        },
        {
            "id": "2b6ed851-767d-4e83-b1b7-a3248398bf54",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "62bc7dde-32cc-4b1d-b457-c2de0d9aad9c",
            "compositeImage": {
                "id": "ebd66a38-4f6c-4b66-bd8e-bf0baa37bd1f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2b6ed851-767d-4e83-b1b7-a3248398bf54",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "08af6ae3-d48c-41d5-b339-2384784cfbf9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2b6ed851-767d-4e83-b1b7-a3248398bf54",
                    "LayerId": "ff399893-d5e0-4f6a-a1aa-5cef6e7c679f"
                }
            ]
        },
        {
            "id": "abc5507b-2663-4c25-af45-98e24159ed55",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "62bc7dde-32cc-4b1d-b457-c2de0d9aad9c",
            "compositeImage": {
                "id": "0c543621-d1fb-4ca6-b81c-f6f9001801ba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "abc5507b-2663-4c25-af45-98e24159ed55",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ab9e172d-2ea1-4418-9b38-c500514e6123",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "abc5507b-2663-4c25-af45-98e24159ed55",
                    "LayerId": "ff399893-d5e0-4f6a-a1aa-5cef6e7c679f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "ff399893-d5e0-4f6a-a1aa-5cef6e7c679f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "62bc7dde-32cc-4b1d-b457-c2de0d9aad9c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}