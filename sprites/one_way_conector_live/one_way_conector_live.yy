{
    "id": "620b744a-017d-4508-b427-56853e292b8c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "one_way_conector_live",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "29887424-d3c3-49f1-aac2-a1f51e6b4662",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "620b744a-017d-4508-b427-56853e292b8c",
            "compositeImage": {
                "id": "0c83d6b9-975b-478c-a87e-8343a0ac3a0f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "29887424-d3c3-49f1-aac2-a1f51e6b4662",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b457dbd5-ddb3-47c2-bc5b-1b0ed7394c4c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "29887424-d3c3-49f1-aac2-a1f51e6b4662",
                    "LayerId": "1a266914-cbed-46cf-9879-d133fbb03b96"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "1a266914-cbed-46cf-9879-d133fbb03b96",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "620b744a-017d-4508-b427-56853e292b8c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}