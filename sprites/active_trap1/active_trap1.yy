{
    "id": "180bebdb-7ec5-4fbe-841a-0e17f8dbf353",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "active_trap1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1a09236b-42fb-4143-9e21-c26bfe8486c9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "180bebdb-7ec5-4fbe-841a-0e17f8dbf353",
            "compositeImage": {
                "id": "686d9a1d-8e73-46ad-aae1-50b9fa59af44",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1a09236b-42fb-4143-9e21-c26bfe8486c9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "08170212-bcfc-44c6-90d6-77a231b11694",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1a09236b-42fb-4143-9e21-c26bfe8486c9",
                    "LayerId": "50a3747c-ee27-4017-9e67-ff95bcb5cab5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "50a3747c-ee27-4017-9e67-ff95bcb5cab5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "180bebdb-7ec5-4fbe-841a-0e17f8dbf353",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}