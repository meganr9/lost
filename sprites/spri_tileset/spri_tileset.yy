{
    "id": "aaa2b98e-725a-47d0-92ba-861555906a41",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spri_tileset",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1023,
    "bbox_left": 0,
    "bbox_right": 1535,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "02f2d5c5-14cf-4820-9831-342cf283d134",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aaa2b98e-725a-47d0-92ba-861555906a41",
            "compositeImage": {
                "id": "3a480da9-ee32-48d1-b7a7-833f97bb59e8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "02f2d5c5-14cf-4820-9831-342cf283d134",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "76aae4b2-f934-4e3c-9f97-251f49fc47a3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "02f2d5c5-14cf-4820-9831-342cf283d134",
                    "LayerId": "4386aed5-cc50-44ab-a761-d8a7a5622ea6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1024,
    "layers": [
        {
            "id": "4386aed5-cc50-44ab-a761-d8a7a5622ea6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "aaa2b98e-725a-47d0-92ba-861555906a41",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1536,
    "xorig": 0,
    "yorig": 0
}