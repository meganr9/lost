{
    "id": "f3475765-52a0-4b4f-9e34-d9bb3f328713",
    "modelName": "GMPath",
    "mvc": "1.0",
    "name": "boss_path",
    "closed": false,
    "hsnap": 0,
    "kind": 1,
    "points": [
        {
            "id": "ae9cb710-0b64-4138-9a6e-9d509ad77654",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1120,
            "y": 928,
            "speed": 100
        },
        {
            "id": "16a47d5d-f86a-4253-9bd7-54f93cddfdc9",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1120,
            "y": 1568,
            "speed": 100
        },
        {
            "id": "3eb315b9-2b8d-4bbf-82f6-f621f05ea5d0",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1216,
            "y": 1600,
            "speed": 100
        },
        {
            "id": "5bc6ff29-76ec-4d78-8f87-e639c6bebbc4",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1824,
            "y": 1600,
            "speed": 100
        },
        {
            "id": "1a3d363e-db65-49fd-929a-442d582a5e89",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1888,
            "y": 1248,
            "speed": 100
        },
        {
            "id": "64304bca-cf91-4dc2-bd99-d2b91f5e91d8",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1856,
            "y": 1152,
            "speed": 100
        },
        {
            "id": "cd36fee4-8752-40b0-920c-305850a695ae",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1504,
            "y": 1152,
            "speed": 100
        },
        {
            "id": "e186e4b1-7886-435a-95a7-92fb8d0b0d83",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1408,
            "y": 1056,
            "speed": 100
        },
        {
            "id": "d545138d-4cde-4d6c-a909-6b0a0166e338",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1440,
            "y": 928,
            "speed": 100
        },
        {
            "id": "84353c31-fc31-47a6-a5fc-a4964ac5fa49",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1632,
            "y": 864,
            "speed": 100
        },
        {
            "id": "86295e9e-90c1-4f73-a94c-701fd05ca558",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1600,
            "y": 704,
            "speed": 100
        },
        {
            "id": "3018b4ee-7d67-4826-9f9b-907b73c049e1",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1632,
            "y": 608,
            "speed": 100
        },
        {
            "id": "52c8779f-b7e1-4c34-bd0d-09fee61da5f0",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1824,
            "y": 576,
            "speed": 100
        },
        {
            "id": "b4b6913a-d5ff-4917-a0c9-40529b9c9547",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1888,
            "y": 416,
            "speed": 100
        },
        {
            "id": "b848e2fc-d0ea-4b2a-aabb-215f584563ed",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1888,
            "y": 160,
            "speed": 100
        },
        {
            "id": "77c3ce77-bf42-41bb-956a-65dad553fd4e",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1600,
            "y": 128,
            "speed": 100
        },
        {
            "id": "31ad9d6e-fd60-4a63-b7fe-b331455f3c83",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1440,
            "y": 288,
            "speed": 100
        },
        {
            "id": "89df9f36-6008-4287-aef8-838a6131b457",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1248,
            "y": 352,
            "speed": 100
        },
        {
            "id": "444937f5-0c1d-4691-a004-b04202009b35",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 736,
            "y": 320,
            "speed": 100
        },
        {
            "id": "506de30d-f543-4264-8395-d8a3d76761b6",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 608,
            "y": 128,
            "speed": 100
        },
        {
            "id": "f4d6b7a1-e821-43c2-9752-3a0eddb17a50",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 224,
            "y": 96,
            "speed": 100
        },
        {
            "id": "62d647f1-9b9f-4c8a-bdb1-3d5c3445546d",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 128,
            "y": 352,
            "speed": 100
        },
        {
            "id": "0ea515d5-1d63-43e1-a2cf-f18a827ff892",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 160,
            "y": 576,
            "speed": 100
        },
        {
            "id": "958718e1-b00d-43e8-b22e-8690ea71067c",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 352,
            "y": 640,
            "speed": 100
        },
        {
            "id": "31e651d5-778a-4751-b4b7-0e237336fb21",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 736,
            "y": 640,
            "speed": 100
        },
        {
            "id": "8a19cff5-e850-4214-bb7c-4c7063743b81",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 800,
            "y": 896,
            "speed": 100
        },
        {
            "id": "eadfcfe2-208e-4aab-b718-9b19713a7745",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 672,
            "y": 992,
            "speed": 100
        },
        {
            "id": "997676e9-da57-4f0a-a023-252e6c68eef3",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 224,
            "y": 960,
            "speed": 100
        },
        {
            "id": "a52358f2-9209-4968-95f4-d3e0c0e10e09",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 128,
            "y": 1184,
            "speed": 100
        },
        {
            "id": "8a446e73-9239-4746-ae4e-745d6a5bbbbe",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 128,
            "y": 1792,
            "speed": 100
        },
        {
            "id": "ea2b336f-7a31-4487-b3f1-c5c783a0e765",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 416,
            "y": 1856,
            "speed": 100
        },
        {
            "id": "f5812eaa-6487-4e04-9475-c28f887031ca",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1856,
            "y": 1856,
            "speed": 100
        }
    ],
    "precision": 4,
    "vsnap": 0
}