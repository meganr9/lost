{
    "id": "73961058-6726-49e8-9cb2-0f56b72046f9",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_mini_game_2_controller",
    "eventList": [
        {
            "id": "15e67456-541a-4545-9553-eee4be8c6b94",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "73961058-6726-49e8-9cb2-0f56b72046f9"
        },
        {
            "id": "4d43b666-bfa0-430c-b09c-16985e40c15e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "73961058-6726-49e8-9cb2-0f56b72046f9"
        },
        {
            "id": "48cfe1c0-bae6-4c65-ade2-10a3646a1a7d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "73961058-6726-49e8-9cb2-0f56b72046f9"
        },
        {
            "id": "7da17865-1470-4fa4-a129-c8f574ea18ea",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 81,
            "eventtype": 5,
            "m_owner": "73961058-6726-49e8-9cb2-0f56b72046f9"
        },
        {
            "id": "58c9c3c2-26ab-440a-9415-d35fae1b3832",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "73961058-6726-49e8-9cb2-0f56b72046f9"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}