/// @description Check if finished
// You can write your code in this editor

for (var i = 0; i < maxWidth; i += 1) {
	for (var j = 0; j < maxHeight; j += 1) {
		if (global.miniGameTwoGrid[i, j].image_index != i+(j*maxWidth)) {
			gameOver = false;
			break;
		}
	}
}
//show_debug_message("gameover"+string(gameOver)+"alreadyRandomized"+string(alreadyRandomized))
if (gameOver && alreadyRandomized) {
	//show_debug_message("!!!!!!!!!!!!!!!!!!!!!!!!!WON!!!")
	alarm_set(1, 2);
	alreadyRandomized = false //prevent multiple calls
}