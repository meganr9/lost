/// @description Create board
// You can write your code in this editor

//Create board
xStart = 100;
yStart = 100;
spriteWidth = sprite_get_width(spr_slider_piece);
spriteHeight = sprite_get_height(spr_slider_piece);
maxWidth = 3;
maxHeight = 3;
gameOver = true;

for (var i = 0; i < maxWidth; i += 1) {
	for (var j = 0; j < maxHeight; j += 1) {
		global.miniGameTwoGrid[i, j] = instance_create_layer(xStart + spriteWidth*i,yStart+spriteHeight*j, "Instances", obj_slider);
		global.miniGameTwoGrid[i, j].image_index = i+(j*maxWidth);
		global.miniGameTwoGrid[i, j].myXLoc = i;
		global.miniGameTwoGrid[i, j].myYLoc = j;
		setupGrid[i, j] = i+(j*maxWidth);
	}
}
alreadyRandomized = false
if (!global.minigameTwoWon) {
	alarm_set(0, 50)
}