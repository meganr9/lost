/// @description Randomize grid

randomize();//randomize random seed
for (var i = 0; i < maxWidth; i += 1) {
	for (var j = 0; j < maxHeight; j += 1) {
		var xPlace = random_range(0, 3)
		var yPlace = random_range(0, 3)
		var placeHolder = setupGrid[xPlace, yPlace]
		//var placeHolder = global.miniGameTwoGrid[xPlace, yPlace].image_index
		//global.miniGameTwoGrid[xPlace, yPlace].image_index = global.miniGameTwoGrid[i, j].image_index
		//global.miniGameTwoGrid[i, j].image_index = placeHolder
		setupGrid[xPlace, yPlace] = setupGrid[i, j]//global.miniGameTwoGrid[i, j].image_index
		setupGrid[i, j] = placeHolder
	}
}

for (var i = 0; i < maxWidth; i += 1) 
{
    for (var j = 0; j < maxHeight; j += 1) 
	{
        checkGrid[i+(j*maxWidth)] = setupGrid[i, j] 
        //checkGrid[i+(j*maxWidth)] = global.miniGameTwoGrid[i, j].image_index 
    }
}
var invCount = 0
for(var i = 0; i < maxWidth*maxHeight-1; i+=1) 
{
    for(var j = i+1; j < maxWidth*maxHeight; j = j+1) 
	{
        if (checkGrid[i] > checkGrid[j]) 
		{
            invCount = invCount + 1
        }
    }
}
show_debug_message("here" + string(invCount))
if (invCount % 2 != 0) 
{
    alarm_set(0, 1)
    show_debug_message("in if " + string(invCount))
    
} 
else 
{
	
	for (var i = 0; i < maxWidth; i += 1) {
		for (var j = 0; j < maxHeight; j += 1) {
			global.miniGameTwoGrid[i, j].image_index = setupGrid[i, j]
		}
	}

    alreadyRandomized = true
}
