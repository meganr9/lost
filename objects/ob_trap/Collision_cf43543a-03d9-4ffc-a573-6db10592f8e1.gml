/// @description Insert description here
// You can write your code in this editor
if inactive
{
	timeline_index = inactive_trap_timeline;
	timeline_running = true;
	if timeline_position > 25
	{
		timeline_position = 0;
		timeline_running = false;
	}
}
else
{
	if set
	{
		timeline_index = set_trap_timeline;
		timeline_running = true;
		if timeline_position > 20
		{
			timeline_position = 0;
			timeline_running = false;
		}
	}
	else
	{
		if active
		{
			timeline_index = active_trap_timeline_player;
			timeline_running = true;
			if timeline_position > 20
			{
				timeline_position = 0;
				timeline_running = false;
			}
		}
	}
}