{
    "id": "d929882d-2f0b-4786-86dd-5e35cde3a500",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "ob_trap",
    "eventList": [
        {
            "id": "ab6947f2-255c-4828-94d3-a2b67183e3ce",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "d929882d-2f0b-4786-86dd-5e35cde3a500"
        },
        {
            "id": "cf43543a-03d9-4ffc-a573-6db10592f8e1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "9f3d3b16-27bd-41a0-85ea-377affbfca07",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "d929882d-2f0b-4786-86dd-5e35cde3a500"
        },
        {
            "id": "22d93136-700a-495f-8a05-eb52e27c9427",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "95bace87-73be-4c61-88b2-271436dea8d3",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "d929882d-2f0b-4786-86dd-5e35cde3a500"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "89fd0eb1-0331-45ff-9b00-4e1437b67d4c",
    "visible": true
}