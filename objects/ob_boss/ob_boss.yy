{
    "id": "95bace87-73be-4c61-88b2-271436dea8d3",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "ob_boss",
    "eventList": [
        {
            "id": "683d4e63-a7ba-45b6-871f-b24b0d43e89d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "95bace87-73be-4c61-88b2-271436dea8d3"
        },
        {
            "id": "c54d67d7-eea4-458f-b5f3-60eaf552eb20",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "95bace87-73be-4c61-88b2-271436dea8d3"
        },
        {
            "id": "93eb2c03-ae3c-4084-883c-1dcaffd5ecb8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 7,
            "m_owner": "95bace87-73be-4c61-88b2-271436dea8d3"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "31619ff7-9882-4fc4-9666-25760d6ef545",
    "visible": true
}