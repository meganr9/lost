{
    "id": "164a72e8-d306-4443-9f22-5fa14a52fe80",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "connector",
    "eventList": [
        {
            "id": "3ec54912-f049-4b11-b2a5-bb91cccc6c47",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "164a72e8-d306-4443-9f22-5fa14a52fe80"
        },
        {
            "id": "bccb1ce7-9c2e-4a07-a2fb-30cc7fc7d300",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "164a72e8-d306-4443-9f22-5fa14a52fe80"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": true,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}