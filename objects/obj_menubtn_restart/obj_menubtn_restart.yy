{
    "id": "541ca093-fab0-461e-84db-af6c1de0ec0d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_menubtn_restart",
    "eventList": [
        {
            "id": "9133c41c-8df1-451f-b8c2-15102d79b87d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "541ca093-fab0-461e-84db-af6c1de0ec0d"
        },
        {
            "id": "4f661348-525a-4fd5-abce-10d970ae3a96",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 6,
            "m_owner": "541ca093-fab0-461e-84db-af6c1de0ec0d"
        },
        {
            "id": "ee36cc39-dbb6-4099-93fd-2423f3d9d373",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "541ca093-fab0-461e-84db-af6c1de0ec0d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "ce468e40-75a5-4168-b941-80e91199917e",
    "visible": true
}