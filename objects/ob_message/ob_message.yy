{
    "id": "08d69053-16f1-4fd6-acf8-168db8ead817",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "ob_message",
    "eventList": [
        {
            "id": "aaaee7e9-1080-4bcd-998b-80bff610e5d1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "08d69053-16f1-4fd6-acf8-168db8ead817"
        },
        {
            "id": "e6e5edf4-48e9-4f3b-acbd-fc6e3cdeee28",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "08d69053-16f1-4fd6-acf8-168db8ead817"
        },
        {
            "id": "a4806bd0-ccc0-427e-b852-34e1d94c97df",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "08d69053-16f1-4fd6-acf8-168db8ead817"
        },
        {
            "id": "73f0c99c-e9bb-4270-8a77-9e6116406536",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "08d69053-16f1-4fd6-acf8-168db8ead817"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}