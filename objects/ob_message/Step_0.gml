/// @description Insert description here
// You can write your code in this editor
if (point_in_rectangle(mouse_x, mouse_y, button_start_x, button_start_y, 
button_start_x+button_width, button_start_y+button_height ) &&(mouse_check_button_released(mb_left)))
{  
	if (canUseAlarm) {
		canUseAlarm = false;
		alarm_set(0, 25);	
		show_debug_message("Called alarm in step method!")
	}
}

if (keyboard_check_released(vk_enter) ) {
	//  Re-activate all other objects and disappear
	instance_destroy();
	instance_activate_all();

	// room_restart();
}