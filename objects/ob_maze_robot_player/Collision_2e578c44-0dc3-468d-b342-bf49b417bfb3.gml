/// @description Insert description here
// You can write your code in this editor
if has_left && has_right
{
	if room_exists(room_next(room)) room_goto_next();
	else
	{
		var inst;
		inst = instance_create_layer(x, y, "Instances", ob_message_end);

		with (inst)
		{
			str_message = "You Win!";
			time = 0;
		}
	}
}
