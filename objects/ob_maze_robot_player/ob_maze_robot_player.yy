{
    "id": "c3b69109-5b64-4a27-b257-0df3b96d3b3c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "ob_maze_robot_player",
    "eventList": [
        {
            "id": "ef158a81-8d77-486e-8190-4978883971b1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "c3b69109-5b64-4a27-b257-0df3b96d3b3c"
        },
        {
            "id": "797eeb89-cfe3-48fd-86ba-9e178f95e046",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 81,
            "eventtype": 5,
            "m_owner": "c3b69109-5b64-4a27-b257-0df3b96d3b3c"
        },
        {
            "id": "fc071730-cae5-46f2-9420-549df776bdcf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "4cff98e3-3ff5-4fe6-bb83-367cc34757eb",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "c3b69109-5b64-4a27-b257-0df3b96d3b3c"
        },
        {
            "id": "468bbb5d-a3ed-4aed-8e0b-e402b50a195a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "139ac7a5-c227-4c1c-b63a-37bef92a6fec",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "c3b69109-5b64-4a27-b257-0df3b96d3b3c"
        },
        {
            "id": "2e578c44-0dc3-468d-b342-bf49b417bfb3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "175a0284-be09-449e-993a-8b62e86c4d5b",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "c3b69109-5b64-4a27-b257-0df3b96d3b3c"
        },
        {
            "id": "e9b899f2-95aa-4b8c-a362-0f4ff3ceb536",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "986e351c-da98-4842-ab33-9394685dda29",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "c3b69109-5b64-4a27-b257-0df3b96d3b3c"
        },
        {
            "id": "dfb6d42c-cdae-4906-aa4d-5e7ab5b02b1b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "371aafcd-a02d-439d-864b-a04bab4655e5",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "c3b69109-5b64-4a27-b257-0df3b96d3b3c"
        },
        {
            "id": "aaeedfc4-a950-4d6b-ac04-958e5ba6dd32",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "c3b69109-5b64-4a27-b257-0df3b96d3b3c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "8b183681-72c9-4f9a-93c5-202f6505ca96",
    "visible": true
}