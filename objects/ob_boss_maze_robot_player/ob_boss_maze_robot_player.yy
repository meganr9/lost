{
    "id": "9f3d3b16-27bd-41a0-85ea-377affbfca07",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "ob_boss_maze_robot_player",
    "eventList": [
        {
            "id": "58530bc9-3a87-4fd7-9960-865f71fa12c6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "9f3d3b16-27bd-41a0-85ea-377affbfca07"
        },
        {
            "id": "3679c184-7a1e-4977-ab2d-73b5251ea69d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 81,
            "eventtype": 5,
            "m_owner": "9f3d3b16-27bd-41a0-85ea-377affbfca07"
        },
        {
            "id": "5ea410fc-b659-4ba2-9fc0-d08df607833c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "175a0284-be09-449e-993a-8b62e86c4d5b",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "9f3d3b16-27bd-41a0-85ea-377affbfca07"
        },
        {
            "id": "d3fe9e3c-4026-44c1-9b76-83f167be877c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "95bace87-73be-4c61-88b2-271436dea8d3",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "9f3d3b16-27bd-41a0-85ea-377affbfca07"
        },
        {
            "id": "b3a55f86-7288-4446-aa22-23dd946be6b1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "9f3d3b16-27bd-41a0-85ea-377affbfca07"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "8b183681-72c9-4f9a-93c5-202f6505ca96",
    "visible": true
}