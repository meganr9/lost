/// @description Insert description here
// You can write your code in this editor
neverWon = true;
game_board[0,0] = instance_create_layer(100, 100, "Instances", one_way);
game_board[1,0] = instance_create_layer(game_board[0,0].x, game_board[0,0].y + game_board[0,0].sprite_height, "Instances", corner);
script_execute(script_rotate_connector, 2, game_board[1,0]);
game_board[2,0] = instance_create_layer(game_board[1,0].x, game_board[1,0].y + game_board[1,0].sprite_height, "Instances", three_way);
game_board[3,0] = instance_create_layer(game_board[2,0].x, game_board[2,0].y + game_board[2,0].sprite_height, "Instances", straight);
script_execute(script_rotate_connector, 1, game_board[3,0]);
game_board[4,0] = instance_create_layer(game_board[3,0].x, game_board[3,0].y + game_board[3,0].sprite_height, "Instances", three_way);
script_execute(script_rotate_connector, 1, game_board[4,0]);

game_board[0,1] = instance_create_layer(game_board[0,0].x + game_board[0,0].sprite_width, game_board[0,0].y, "Instances", three_way);
script_execute(script_rotate_connector, 3, game_board[0, 1]);
game_board[1,1] = instance_create_layer(game_board[0,1].x, game_board[0,1].y + game_board[0,1].sprite_height, "Instances", three_way);
script_execute(script_rotate_connector, 1, game_board[1, 1]);
game_board[2,1] = instance_create_layer(game_board[1,1].x, game_board[1,1].y + game_board[1,1].sprite_height, "Instances", corner);
script_execute(script_rotate_connector, 2, game_board[2, 1]);
game_board[3,1] = instance_create_layer(game_board[2,1].x, game_board[2,1].y + game_board[2,1].sprite_height, "Instances", corner);
game_board[4,1] = instance_create_layer(game_board[3,1].x, game_board[3,1].y + game_board[3,1].sprite_height, "Instances", one_way);
script_execute(script_rotate_connector, 2, game_board[4, 1]);

game_board[0,2] = instance_create_layer(game_board[0,1].x + game_board[0,1].sprite_width, game_board[0,1].y, "Instances", corner);
game_board[1,2] = instance_create_layer(game_board[0,2].x, game_board[0,2].y + game_board[0,2].sprite_height, "Instances", four_way);
game_board[2,2] = instance_create_layer(game_board[1,2].x, game_board[1,2].y + game_board[1,2].sprite_height, "Instances", three_way);
script_execute(script_rotate_connector, 3, game_board[2, 2]);
game_board[3,2] = instance_create_layer(game_board[2,2].x, game_board[2,2].y + game_board[2,2].sprite_height, "Instances", three_way);
game_board[3, 2].is_live = true;
script_execute(script_rotate_connector, 2, game_board[3, 2]);
game_board[4,2] = instance_create_layer(game_board[3,2].x, game_board[3,2].y + game_board[3,2].sprite_height, "Instances", corner);
game_board[4, 2].is_live = true;
script_execute(script_rotate_connector, 1, game_board[4, 2]);

game_board[0,3] = instance_create_layer(game_board[0,2].x + game_board[0,2].sprite_width, game_board[0,2].y, "Instances", corner);
script_execute(script_rotate_connector, 1, game_board[0, 3]);
game_board[1,3] = instance_create_layer(game_board[0,3].x, game_board[0,3].y + game_board[0,3].sprite_height, "Instances", three_way);
game_board[2,3] = instance_create_layer(game_board[1,3].x, game_board[1,3].y + game_board[1,3].sprite_height, "Instances", corner);
script_execute(script_rotate_connector, 2, game_board[2, 3]);
game_board[3,3] = instance_create_layer(game_board[2,3].x, game_board[2,3].y + game_board[2,3].sprite_height, "Instances", corner);
game_board[3, 3].is_live = true;
game_board[4,3] = instance_create_layer(game_board[3,3].x, game_board[3,3].y + game_board[3,3].sprite_height, "Instances", four_way);
game_board[4, 3].is_live = true;

game_board[0,4] = instance_create_layer(game_board[0,3].x + game_board[0,3].sprite_width, game_board[0,3].y, "Instances", straight);
script_execute(script_rotate_connector, 1, game_board[0, 4]);
game_board[1,4] = instance_create_layer(game_board[0,4].x, game_board[0,4].y + game_board[0,4].sprite_height, "Instances", corner);
script_execute(script_rotate_connector, 1, game_board[1, 4]);
game_board[2,4] = instance_create_layer(game_board[1,4].x, game_board[1,4].y + game_board[1,4].sprite_height, "Instances", one_way);
script_execute(script_rotate_connector, 2, game_board[2, 4]);
game_board[3,4] = instance_create_layer(game_board[2,4].x, game_board[2,4].y + game_board[2,4].sprite_height, "Instances", corner);
script_execute(script_rotate_connector, 2, game_board[3, 4]);
game_board[4,4] = instance_create_layer(game_board[3,4].x, game_board[3,4].y + game_board[3,4].sprite_height, "Instances", straight);


instance_create_layer(game_board[1,1].x, game_board[0,0].y - game_board[0,0].sprite_height, "Instances", obj_connectorend);
instance_create_layer(game_board[1,3].x, game_board[0,0].y - game_board[0,0].sprite_height, "Instances", obj_connectorend);
instOne = instance_create_layer(game_board[1,3].x, game_board[4,4].y + game_board[0,0].sprite_height, "Instances", obj_connectorend);
instOne.image_angle = 180
instance_create_layer(game_board[1,4].x, game_board[0,0].y - game_board[0,0].sprite_height, "Instances", obj_connectorend);


instTwo = instance_create_layer(game_board[1,0].x - game_board[0,0].sprite_width, game_board[4,4].y, "Instances", obj_connectorend);
instThree = instance_create_layer(game_board[4,4].x + game_board[0,0].sprite_width, game_board[4,4].y, "Instances", obj_connectorend);
instTwo.image_angle = 90
instThree.image_angle = -90
if (!global.minigameOneWon) {
	instance_create_layer(0, 0, "Instances", ob_message)
} else {
	instance_destroy(self);
}
