{
    "id": "115ef38f-be49-4bfa-9a89-63cc86aa2a69",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "object_mini_game_1_controller",
    "eventList": [
        {
            "id": "b7b0a340-9ad5-4952-a849-2cb04f0a4328",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "115ef38f-be49-4bfa-9a89-63cc86aa2a69"
        },
        {
            "id": "65cdbeca-28ed-4b29-96e0-32c03d58481e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "115ef38f-be49-4bfa-9a89-63cc86aa2a69"
        },
        {
            "id": "784f1a1a-6431-4e87-b100-3238b5292690",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "115ef38f-be49-4bfa-9a89-63cc86aa2a69"
        },
        {
            "id": "e9b92bdb-79b9-45db-a1c7-b66087cce37c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "115ef38f-be49-4bfa-9a89-63cc86aa2a69"
        },
        {
            "id": "380b40f7-29f6-4794-a18d-18029f43eda1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 81,
            "eventtype": 10,
            "m_owner": "115ef38f-be49-4bfa-9a89-63cc86aa2a69"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}