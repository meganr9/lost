{
    "id": "986e351c-da98-4842-ab33-9394685dda29",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "ob_maze_robot_h",
    "eventList": [
        {
            "id": "01240f44-6316-4510-ba3b-1ce2dc01ee00",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "986e351c-da98-4842-ab33-9394685dda29"
        },
        {
            "id": "bfbd31ea-ec8e-4467-995d-1f4d3485a5e5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "986e351c-da98-4842-ab33-9394685dda29"
        },
        {
            "id": "c10a3e91-52c4-4ba1-a43e-8592e55a9d65",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "51184f55-7a6b-4e00-a4fa-809c3eba48c0",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "986e351c-da98-4842-ab33-9394685dda29"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "39d62d34-8030-400d-9fce-1e8d66a0451d",
    "visible": true
}