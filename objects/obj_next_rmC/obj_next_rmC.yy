{
    "id": "cb59ebba-e601-425d-b8a0-0a8f02864b5e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_next_rmC",
    "eventList": [
        {
            "id": "d33d1a90-c820-46de-a305-e67a32515592",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "872dcb4b-a9ad-413d-93d6-164cb1b5c740",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "cb59ebba-e601-425d-b8a0-0a8f02864b5e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": true,
    "spriteId": "5b8a507d-f5fa-4102-a4fb-80e83ab40ce0",
    "visible": false
}