{
    "id": "d508f668-3c5b-486d-b356-6f38a826fc73",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "three_way",
    "eventList": [
        {
            "id": "d9be9eb3-37a4-458b-aa17-3488afb049ea",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "d508f668-3c5b-486d-b356-6f38a826fc73"
        },
        {
            "id": "d4142db2-dfc8-4bda-9fe0-dfe05451c720",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "d508f668-3c5b-486d-b356-6f38a826fc73"
        },
        {
            "id": "61b35e4a-0510-4130-92fd-06566055f71f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "d508f668-3c5b-486d-b356-6f38a826fc73"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "164a72e8-d306-4443-9f22-5fa14a52fe80",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "c4d388d2-eb3f-4b44-8e80-c4c14dbd9a7d",
    "visible": true
}