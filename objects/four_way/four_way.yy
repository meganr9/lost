{
    "id": "ea74bcb5-e129-43c2-8fd2-dfa6398d1179",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "four_way",
    "eventList": [
        {
            "id": "d612eb94-7135-4f57-943c-20c7908c7d1f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "ea74bcb5-e129-43c2-8fd2-dfa6398d1179"
        },
        {
            "id": "6a4ccaea-fefb-4ba8-8720-d86bf8368c7b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "ea74bcb5-e129-43c2-8fd2-dfa6398d1179"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "164a72e8-d306-4443-9f22-5fa14a52fe80",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "848e46c0-fe5a-482e-ab0d-413260518a6e",
    "visible": true
}