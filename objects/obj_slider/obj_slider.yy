{
    "id": "1da7ed62-08d1-4aa9-a888-14c5e27f0fb3",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_slider",
    "eventList": [
        {
            "id": "2bd2fa2d-3479-4197-a805-1f175a0b57f0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "1da7ed62-08d1-4aa9-a888-14c5e27f0fb3"
        },
        {
            "id": "5b12eb54-f055-4453-81e0-21301ac4dd37",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 6,
            "m_owner": "1da7ed62-08d1-4aa9-a888-14c5e27f0fb3"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "62bc7dde-32cc-4b1d-b457-c2de0d9aad9c",
    "visible": true
}