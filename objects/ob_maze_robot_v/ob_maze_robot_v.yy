{
    "id": "371aafcd-a02d-439d-864b-a04bab4655e5",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "ob_maze_robot_v",
    "eventList": [
        {
            "id": "a2319e5a-9f77-4bea-8041-3ce67df3f835",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "371aafcd-a02d-439d-864b-a04bab4655e5"
        },
        {
            "id": "7787dbac-11db-4b62-9006-a320586ee6f7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "371aafcd-a02d-439d-864b-a04bab4655e5"
        },
        {
            "id": "4040880c-8e71-4697-bba6-092e648bb062",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "51184f55-7a6b-4e00-a4fa-809c3eba48c0",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "371aafcd-a02d-439d-864b-a04bab4655e5"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "39d62d34-8030-400d-9fce-1e8d66a0451d",
    "visible": true
}