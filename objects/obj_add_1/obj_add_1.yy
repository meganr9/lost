{
    "id": "f45b07fc-5fc6-48c1-9631-2cc5411dbad4",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_add_1",
    "eventList": [
        {
            "id": "f7f6d78a-284f-4680-a79a-cbef74102b21",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "f45b07fc-5fc6-48c1-9631-2cc5411dbad4"
        },
        {
            "id": "8706c2ca-89e2-4024-9fac-1b256d058652",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "f45b07fc-5fc6-48c1-9631-2cc5411dbad4"
        },
        {
            "id": "62dd2207-9839-465f-be78-30be29fb0989",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "f45b07fc-5fc6-48c1-9631-2cc5411dbad4"
        },
        {
            "id": "8339184d-3ca7-4ae1-b532-1e5c2167fd15",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "f45b07fc-5fc6-48c1-9631-2cc5411dbad4"
        },
        {
            "id": "1d392164-c6c6-4609-86a1-62d053e53371",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "f45b07fc-5fc6-48c1-9631-2cc5411dbad4"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "872dcb4b-a9ad-413d-93d6-164cb1b5c740",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "c4d7406f-0fb6-40b9-b680-92bfaaf7b69d",
    "visible": true
}