{
    "id": "4cff98e3-3ff5-4fe6-bb83-367cc34757eb",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "ob_arm_left",
    "eventList": [
        {
            "id": "9750402b-6b08-4826-a916-4063ff7a42d6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "872dcb4b-a9ad-413d-93d6-164cb1b5c740",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "4cff98e3-3ff5-4fe6-bb83-367cc34757eb"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "e3444259-29a1-43b2-9358-3f69ea9a49ae",
    "visible": true
}