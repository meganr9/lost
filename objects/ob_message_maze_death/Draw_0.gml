/// @description Insert description here
// You can write your code in this editor
//  Use this to create a typewriter effect
speed = 0.5;

if (time <  string_length(str_message))
{
    time += speed;
    str_message_drawn = string_copy(str_message, 0, time);
}


//  TODO: Set the font here
//draw_set_font

//  Set the dimensions of the box based on the size of the message
box_width = string_width(string_hash_to_newline(str_message)) + 10;
box_height = string_height(string_hash_to_newline(str_message)) + 10;

//  Include room for an "OK" button
button_width = string_width(string_hash_to_newline("OK")) + 4;
button_height = string_height(string_hash_to_newline("OK")) + 4;
box_width += button_width;
box_height += button_height;


//  Place the box at the center of the room
box_x = 0;
box_y = 0;

if (view_enabled)
{
    box_x = camera_get_view_x( view_camera[0] ) + (camera_get_view_width( view_camera[0] )/2) - (box_width/2);
    box_y = camera_get_view_y( view_camera[0] ) + (camera_get_view_height( view_camera[0] )/2) - (box_height/2);
}
else
{
    box_x = (room_width/2) - (box_width/2);
    box_y = (room_height/2) - (box_height/2);
}

//  Display the box
draw_sprite_ext(spr_dialog_background,0, box_x, box_y, box_width/32, box_height/32, 0, c_white, 1);

//  Display the "OK" button
button_start_x = box_x + (box_width/2) - (button_width/2);
button_start_y = box_y + box_height - button_height;
draw_rectangle(button_start_x, button_start_y, button_start_x+button_width, button_start_y+button_height, 1);
draw_text(button_start_x + 2, button_start_y + 2, string_hash_to_newline("OK"));

//  Draw a border
draw_set_color(c_gray);
draw_rectangle(box_x, box_y, box_x+box_width, box_y+box_height, 1);

//  Display the message
draw_text(box_x + 5, box_y + 5, string_hash_to_newline(str_message_drawn));



