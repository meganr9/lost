{
    "id": "161605ce-201d-487a-84ed-75b3160f7cb1",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "ob_message_maze_death",
    "eventList": [
        {
            "id": "be0f2ea0-4dc4-4712-96a9-d148034c48ad",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "161605ce-201d-487a-84ed-75b3160f7cb1"
        },
        {
            "id": "0493e8e8-fbef-4dfe-a554-b86f3184fc98",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "161605ce-201d-487a-84ed-75b3160f7cb1"
        },
        {
            "id": "bf39a762-ca2b-4fca-884b-21f0f4c59d31",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "161605ce-201d-487a-84ed-75b3160f7cb1"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "a2095d69-0622-4249-8054-651f719ab2c8",
    "visible": true
}