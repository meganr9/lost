{
    "id": "04769629-b48c-4ced-9a26-763ce943b4f3",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "ob_message_end",
    "eventList": [
        {
            "id": "0dad985f-164d-4e41-913e-5c89bd1fe8da",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "04769629-b48c-4ced-9a26-763ce943b4f3"
        },
        {
            "id": "392a20e5-995d-49a0-8488-348eaaa797a2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "04769629-b48c-4ced-9a26-763ce943b4f3"
        },
        {
            "id": "dff4c0bb-3e81-48ee-8089-100aeae50bb6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "04769629-b48c-4ced-9a26-763ce943b4f3"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "a2095d69-0622-4249-8054-651f719ab2c8",
    "visible": true
}