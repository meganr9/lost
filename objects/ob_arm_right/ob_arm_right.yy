{
    "id": "139ac7a5-c227-4c1c-b63a-37bef92a6fec",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "ob_arm_right",
    "eventList": [
        {
            "id": "633bdef6-a484-46d5-9b36-6dda11cb2495",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "872dcb4b-a9ad-413d-93d6-164cb1b5c740",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "139ac7a5-c227-4c1c-b63a-37bef92a6fec"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "60ea6949-62b1-4e30-9007-90a8167eb556",
    "visible": true
}