{
    "id": "a1a4b316-86a1-40c1-84e0-f2b5cff8df42",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_player_up",
    "eventList": [
        {
            "id": "6052bd07-04c0-4f52-90da-ccf82f07d8cd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "a1a4b316-86a1-40c1-84e0-f2b5cff8df42"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "565dcac8-1f61-4a50-addf-06c8368973c4",
    "visible": true
}