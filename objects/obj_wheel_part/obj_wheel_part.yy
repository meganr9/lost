{
    "id": "279779f4-b09f-47a5-a41e-734e09988e7f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_wheel_part",
    "eventList": [
        {
            "id": "03d789cc-8826-4741-abd8-6a164f64e7f3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "872dcb4b-a9ad-413d-93d6-164cb1b5c740",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "279779f4-b09f-47a5-a41e-734e09988e7f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "4856ec22-efce-4789-bfee-485c6f951caa",
    "visible": true
}