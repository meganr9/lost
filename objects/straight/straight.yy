{
    "id": "2090ae96-3852-43ff-86f1-4f0d35d13dff",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "straight",
    "eventList": [
        {
            "id": "640991b1-cb62-4745-abf8-18a20c924f29",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "2090ae96-3852-43ff-86f1-4f0d35d13dff"
        },
        {
            "id": "b3254bf3-a134-4ab8-90c7-3d95fd5b8489",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "2090ae96-3852-43ff-86f1-4f0d35d13dff"
        },
        {
            "id": "1c04f67a-2501-4407-93e5-262f56b24f4e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "2090ae96-3852-43ff-86f1-4f0d35d13dff"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "164a72e8-d306-4443-9f22-5fa14a52fe80",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "8174420a-4711-4071-8d4a-412255867f77",
    "visible": true
}