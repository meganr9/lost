{
    "id": "41179aee-2b0c-499b-b231-c6cfe1ee525a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_body_part1",
    "eventList": [
        {
            "id": "757048fa-5da0-4a8d-b18e-8ee335289c36",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "872dcb4b-a9ad-413d-93d6-164cb1b5c740",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "41179aee-2b0c-499b-b231-c6cfe1ee525a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "e1cd2247-74eb-48c3-8754-588315766421",
    "visible": true
}