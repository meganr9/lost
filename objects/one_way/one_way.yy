{
    "id": "756ec045-4b17-4574-a0d7-f4fff6e3b007",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "one_way",
    "eventList": [
        {
            "id": "7680723d-eba3-4def-85a2-588e2c99f1fd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "756ec045-4b17-4574-a0d7-f4fff6e3b007"
        },
        {
            "id": "756092e0-59c3-49d3-94f4-bd32d166f1e4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "756ec045-4b17-4574-a0d7-f4fff6e3b007"
        },
        {
            "id": "a2b0be23-c87e-49e2-8974-08874f4cf1d4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "756ec045-4b17-4574-a0d7-f4fff6e3b007"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "164a72e8-d306-4443-9f22-5fa14a52fe80",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "520bdc18-0244-4bfc-ae4b-90ba4e62e463",
    "visible": true
}