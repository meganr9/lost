{
    "id": "ac860cbe-e23d-42a7-95ef-4b9d8c6235c2",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_previous_rm",
    "eventList": [
        {
            "id": "334423f4-4f04-4022-ac7a-020152ebcebe",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "ac860cbe-e23d-42a7-95ef-4b9d8c6235c2"
        },
        {
            "id": "c5c7c69d-c9bf-4d28-88ac-bb0035def379",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "872dcb4b-a9ad-413d-93d6-164cb1b5c740",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "ac860cbe-e23d-42a7-95ef-4b9d8c6235c2"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": true,
    "spriteId": "1852803d-1226-4987-8c31-6ae20660782d",
    "visible": false
}