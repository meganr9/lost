/// @description Insert description here
// You can write your code in this editor
if image_angle == 90
{
	top = 0;
	bottom = 1;
	left = 0;
	right = 1;
}
else
{
	if image_angle == 180
	{
		top = 1;
		bottom = 0;
		left = 0;
		right = 1;
	}
	else
	{
		if image_angle == 270
		{
			top = 1;
			bottom = 0;
			left = 1;
			right = 0;
		}
		else
		{
			if image_angle == 0
			{
				top = 0;
				bottom = 1;
				left = 1;
				right = 0;
			}
		}
	}
}


if instance_position(x + 30, y, four_way) != noone
{
	if instance_position(x + 30, y, four_way).left == 1 && right == 1
	{
		object_set_sprite(1, corner_way_conector_live);
		is_live = true;
	}
	else
	{
		object_set_sprite(1, corner_way_conector);
		is_live = false;
	}
}