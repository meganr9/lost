{
    "id": "bca5e35e-7d5d-4457-bfff-c2bcd2bb0f1a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "corner",
    "eventList": [
        {
            "id": "bdc6f15f-48e2-4538-bd2e-434c2411487a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "bca5e35e-7d5d-4457-bfff-c2bcd2bb0f1a"
        },
        {
            "id": "55b7f77e-4203-4f98-a654-81bd41155c60",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "bca5e35e-7d5d-4457-bfff-c2bcd2bb0f1a"
        },
        {
            "id": "61cbb984-3d7b-446b-8069-bb92d8c9b846",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "bca5e35e-7d5d-4457-bfff-c2bcd2bb0f1a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "164a72e8-d306-4443-9f22-5fa14a52fe80",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "6ff40d3f-9000-4526-9fe1-32f736e583f4",
    "visible": true
}