{
    "id": "51184f55-7a6b-4e00-a4fa-809c3eba48c0",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "bumper",
    "eventList": [
        {
            "id": "a1c84b12-178c-46d6-81e0-9f50f41b461a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "872dcb4b-a9ad-413d-93d6-164cb1b5c740",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "51184f55-7a6b-4e00-a4fa-809c3eba48c0"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": true,
    "spriteId": "a6ef1554-56f0-48c2-8862-90304e6be24b",
    "visible": false
}