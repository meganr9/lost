{
    "id": "117ffa30-a4ec-4398-b248-fc4979bba1a2",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "ob_maze_wall",
    "eventList": [
        {
            "id": "506edca0-e4f9-48a0-a1a7-4108630e3516",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "872dcb4b-a9ad-413d-93d6-164cb1b5c740",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "117ffa30-a4ec-4398-b248-fc4979bba1a2"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": true,
    "spriteId": "c94585b8-0ed4-4ec4-8707-3ca94cb3971a",
    "visible": true
}