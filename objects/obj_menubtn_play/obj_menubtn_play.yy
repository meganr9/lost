{
    "id": "b1ae89e4-310b-49ef-a1de-dfc4660b2e5f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_menubtn_play",
    "eventList": [
        {
            "id": "833910ce-0d1c-4435-a90d-6e3076c34735",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b1ae89e4-310b-49ef-a1de-dfc4660b2e5f"
        },
        {
            "id": "32c5eaa3-c835-4ba4-a59b-f365f2efae40",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 6,
            "m_owner": "b1ae89e4-310b-49ef-a1de-dfc4660b2e5f"
        },
        {
            "id": "c6222194-be2a-41cd-a90a-c65a13bec3a4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "b1ae89e4-310b-49ef-a1de-dfc4660b2e5f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "ce468e40-75a5-4168-b941-80e91199917e",
    "visible": true
}