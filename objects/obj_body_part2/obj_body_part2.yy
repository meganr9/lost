{
    "id": "dd71e96c-c92d-4023-84a3-466921880ed8",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_body_part2",
    "eventList": [
        {
            "id": "d9158b8e-fb87-4b47-a7f1-a96dcc81ecd5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "872dcb4b-a9ad-413d-93d6-164cb1b5c740",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "dd71e96c-c92d-4023-84a3-466921880ed8"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "44d2bd1d-0fef-4be4-9005-d6c4da67940e",
    "visible": true
}