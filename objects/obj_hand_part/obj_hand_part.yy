{
    "id": "6b00e70b-a620-4247-a4c3-592f10e36aa2",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_hand_part",
    "eventList": [
        {
            "id": "5ff50a0a-7743-42c0-b127-ac8d6214ec41",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "872dcb4b-a9ad-413d-93d6-164cb1b5c740",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "6b00e70b-a620-4247-a4c3-592f10e36aa2"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "b9866967-1d07-49c4-b272-1ac97867aa97",
    "visible": true
}