{
    "id": "872dcb4b-a9ad-413d-93d6-164cb1b5c740",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_head_profile",
    "eventList": [
        {
            "id": "94321917-8923-408a-b9a7-d5c9048ea7b2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "872dcb4b-a9ad-413d-93d6-164cb1b5c740"
        },
        {
            "id": "50debc90-f66f-4f89-ad16-370c06f000b7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "872dcb4b-a9ad-413d-93d6-164cb1b5c740"
        },
        {
            "id": "090393d6-5e7d-484e-8c92-4c7baae97f57",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "f13eecc7-b6b3-451a-9ea0-a82f5cad1970",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "872dcb4b-a9ad-413d-93d6-164cb1b5c740"
        },
        {
            "id": "48a82587-db69-4dcb-8196-0b42f61fbdc3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "4af3ba30-219b-474e-ab87-2d068559951a",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "872dcb4b-a9ad-413d-93d6-164cb1b5c740"
        },
        {
            "id": "75f631a4-1d8d-4cae-ad41-247e2369292c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "ac860cbe-e23d-42a7-95ef-4b9d8c6235c2",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "872dcb4b-a9ad-413d-93d6-164cb1b5c740"
        },
        {
            "id": "1c9355b0-57fd-4093-99d2-5fbb99a0940e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "51184f55-7a6b-4e00-a4fa-809c3eba48c0",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "872dcb4b-a9ad-413d-93d6-164cb1b5c740"
        },
        {
            "id": "821430bb-5add-49d5-851d-6209870e9c1e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "175a0284-be09-449e-993a-8b62e86c4d5b",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "872dcb4b-a9ad-413d-93d6-164cb1b5c740"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "575e847a-4c51-41bf-a16d-64f01adef958",
    "visible": true
}