/// @description Insert description here
// You can write your code in this editor


var right = keyboard_check(vk_right);
var left = keyboard_check(vk_left);
var up = keyboard_check(vk_up);
var down = keyboard_check(vk_down);
hsp = (right - left) * move ;
vsp = (down - up) * move ;
if (global.canMove) {
	x = x + hsp;
	y = y + vsp;



	if (place_meeting(x+hsp, y, ob_maze_wall)){
		while(!place_meeting(x+sign(hsp), y, ob_maze_wall)){
			x = x + sign(hsp) -7;
		}
		hsp = 0;
	}

	x = x + hsp;

	if (place_meeting(x, y+vsp, ob_maze_wall)){
		while(!place_meeting(x, y+sign(vsp), ob_maze_wall)){
			y = y + sign(vsp) - 7;
		}
		vsp = 0;
	}

	y = y + vsp;
} else if (hsp > 0 || vsp > 0) {
	
	message = instance_create_layer(room_width/2, room_height/2, "Instances", ob_message);
	message.str_message = 0
	message.str_message[0] = "Complete the puzzle to connect your brain wires!#Hit Enter"
			
}

if (room != rm_menu) {
	global.prevRoom = room;
}



